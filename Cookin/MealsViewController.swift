//
//  MealsViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

enum LoadMode {
    case CheifMeals
    case searchMeals
    
    var Default: LoadMode {
        return .searchMeals
    }
}



import UIKit
import CSStickyHeaderFlowLayout
import SDWebImage
import DZNEmptyDataSet
import CCMPopup


class MealsViewController: BaseViewController,MealsCellDelagete {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var loadMode :LoadMode = LoadMode.searchMeals
    var refresher:UIRefreshControl!

    var headerHight:CGFloat!=0.0
    var data = [Meal]()
    var isCheifWithoutData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresher = UIRefreshControl()
        refresher.tintColor = UIColor.white
        refresher.backgroundColor = UIColor(fromARGBHexString: "01B0F0")
        
        refresher.attributedTitle = NSAttributedString(string: "Pull To Refresh")
        refresher.bounds  = refresher.bounds.offsetBy(dx: 0, dy: loadMode == .CheifMeals ? 25 : 0)
        refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.collectionView!.refreshControl = refresher
        } else {
            self.collectionView!.addSubview(refresher)
        }
        loadData()
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
        
        headerHight =  378
        
        //        if #available(iOS 10.0, *) {
//        if #available(iOS 10.0, *) {
//            self.collectionView?.isPrefetchingEnabled = false
//        } else {
//            // Fallback on earlier versions
//        }
       
        
        
    }
    
    
    func loadData()  {
        if loadMode == .CheifMeals
        {
            self.collectionView.register(UINib(nibName: "HeaderCheifProfileCell",bundle: nil), forCellWithReuseIdentifier: "HeaderCheifProfileCell")
            self.collectionView?.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCheifProfileCell")
            self.collectionView.reloadData()
            isCheifWithoutData = true
            self.data.append(Meal(JSON: [String: AnyObject]())!)
            
            UserModel.GetCheifMeals(data: ["MapLat": CurrentUserLocation!.coordinate.latitude as AnyObject,"MapLng": CurrentUserLocation!.coordinate.latitude as AnyObject], callback: { (dataa, error, errCode) in
                let x = (dataa as! [String: AnyObject])["Data"] as! [[String: AnyObject]]
                self.data.removeAll()
                for i in x
                {
                    self.data.append(Meal(JSON: i)!)
                }
                if self.data.count > 0
                {
                    self.isCheifWithoutData = false
                    
                }
                else
                {
                    self.data.append(Meal(JSON: [String: AnyObject]())!)
                    self.isCheifWithoutData = true
                    
                    
                }
                self.refresher.endRefreshing()

                self.collectionView.reloadData()
            })
        }
        else
        {
            
            UserModel.SearchMeals(data: ["MapLat": CurrentUserLocation!.coordinate.latitude as AnyObject,"MapLng": CurrentUserLocation!.coordinate.latitude as AnyObject], callback: { (dataa, error, errCode) in
                let x = dataa as! [[String: AnyObject]]
                self.data.removeAll()
                for i in x
                {
                    self.data.append(Meal(JSON: i)!)
                }
                self.isCheifWithoutData = false
                self.collectionView.reloadData()
                self.refresher.endRefreshing()
            })
            
        }
        self.refresher.endRefreshing()

    }
    
        override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.layoutIfNeeded()
        
    }
    
        func ShowMealImages(_ cell: MealsCell) {
            performSegue(withIdentifier: "showmealsImages", sender: cell)
        }
        func ShareMeal(_ cell: MealsCell) {
            self.navigationController?.shareImageAndText(image: cell.mealImage.image!, text: cell.mealDescription!.text!)
        }
    override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)
        UserModel.GetProfile { (data, error, errCode) in
            if !super.handleNetworkError(data ,msg: error, errorCode: 0 ,username: "") {
                let dic = (data as! [String: AnyObject])
                LoggedUserProfile = UserProfile(JSON: dic)
                self.collectionView.reloadData()
            }
        }
                self.tabBarController?.tabBar.isHidden = false
        
            }
        
            override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                if segue.identifier == "showmealsImages" {
                    let vc = segue.destination as! MealImagesViewController
                    vc.meal = (sender as! MealsCell).service
                }
                else if segue.identifier == "MealsDetails"
                {
                        let vc = segue.destination as! MealDetailsTViewController
                        vc.meal = (sender as! MealsCell).service
                }
                else if segue.identifier == "Performance"
                {
                    segue.ccmPopUp(height: 380)
                    segue.destination.view.AddBorderWithColor(radius: 15, borderWidth: 4, color: "01B0F0")
                }
            }
}


extension MealsViewController : UICollectionViewDataSource  {
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MealsCell", for: indexPath) as! MealsCell
        cell.delegate = self
        cell.service = data[indexPath.row]
        
        return cell
        
        
    }
    
    
}


extension MealsViewController :UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,HeaderCheifProfileCellDelagete{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if loadMode == .CheifMeals
        {
        return  UIEdgeInsetsMake(10,0,0,0)
        }
        return  UIEdgeInsetsMake(0,0,0,0)

        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if loadMode == .CheifMeals
        {
            return CGSize(width: self.collectionView.frame.size.width, height: headerHight)
        }
        return CGSize(width: self.collectionView.frame.size.width, height: 0)

        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        switch kind {
//            
//        case UICollectionElementKindSectionHeader:
//            
        
            let cell = self.collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCheifProfileCell", for: indexPath)
            for v in cell.subviews {
                v.removeFromSuperview()
            }
            
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCheifProfileCell", for: indexPath) as! HeaderCheifProfileCell
            cell2.delegate = self
            if LoggedUserProfile != nil
            {
            cell2.service = LoggedUserProfile!
            }
            cell2.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: headerHight)
            
            cell.addSubview(cell2)
            
            
            return cell

            
//        default:
//            
//            assert(false, "Unexpected element kind")
//        }
//        
//          return nil
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if (Constants.ScreenHeight / 3 ) > 260
//        {
//            return CGSize(width: Constants.ScreenWidth, height: Constants.ScreenHeight/3);
//            
//        }
        if isCheifWithoutData
        {
            return CGSize(width: Constants.ScreenWidth, height: 0)

        }
        return CGSize(width: Constants.ScreenWidth, height: 260)
    }
   
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if data.count == 0 && isCheifWithoutData == false
        {
            return true
        }
        return false
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string = "No Data Yet"
        //        let attributes = [NSFontAttributeName: , NSForegroundColorAttributeName: Color.DarkGray]
        let attributedString = NSAttributedString(string: string, attributes: nil)
        return attributedString
        
    }
    
    
    ///// HeaderProfile Cell Delegate Methods
    
    func ShowLikes(_ cell: HeaderCheifProfileCell) {
        performSegue(withIdentifier: "Likes", sender: cell)
        
    }
    func ShowNewMeal(_ cell: HeaderCheifProfileCell) {
        performSegue(withIdentifier: "NewMeal", sender: cell)

    }
    func ShowProfile(_ cell: HeaderCheifProfileCell) {
        performSegue(withIdentifier: "Profile", sender: cell)

    }
    func ShowSetting(_ cell: HeaderCheifProfileCell) {
        performSegue(withIdentifier: "Settings", sender: cell)

    }
    func ShowPerformance(_ cell: HeaderCheifProfileCell) {
        performSegue(withIdentifier: "Performance", sender: cell)

    }

  }


