//
//  ForgetPasswordViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/9/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import UIKit


class ForgetPasswordViewController : BaseViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var ForgetBtn: UIButton!
    //MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ForgetBtn.Curvyimage()
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChangePassword(_:)), for: UIControlEvents.editingChanged)
    }
    
    //MARK: Actions
    @IBAction func togglePasswordSecureState() {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        
        //        if passwordTextField.isFirstResponder() {
        //            passwordTextField.becomeFirstResponder()
        //        }
    }
    
    @IBAction func loginButton() {
        //        if (!self.checkForRequiredFields()) {
        //            AlertUtility.showErrorAlert("Enter Mobile Number and password")
        //
        //            return
        //        }
        
        if (!self.validateAll()) {
            return
        }
        UserModel.ForgetPassword(data: ["MobileNumber": passwordTextField.text! as AnyObject]) { (user, err, errCode) in
            
            if !super.handleNetworkError(user ,msg: err, errorCode: 0 ,username: "") {
                //LOGIC
                print("success")
                print(user!)
                AlertUtility.showSuccessAlert((user as! [String: AnyObject])["EnglishMessage"] as! String)
                self.navigationController?.popViewController(animated: true)
                //                self.navigateToHome()
            }
        }
    }
    
    
    
    //MARK: private functions
    
    
    func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
        } else {
            //            textField.hideExclamationMark()
            
            
        }
    }
    func textFieldDidChangePassword(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
            //            showPasswordBtn.isHidden = true
        } else {
            //            textField.hideExclamationMark()
            //            showPasswordBtn.isHidden = false
            
            
        }
    }
    override func SetupView() {
        self.navigationBarStyle = "default"
        super.SetupView()
        
    }
    
    
    
    
    //MARK: public functions
    
    
    //MARK: Validator
    fileprivate func checkForRequiredFields() -> Bool {
        var res = true
        
        // Check for email
        
        
        // Check for password
        if (passwordTextField.text?.isEmpty == true) {
            //            passwordTextField.showExclamationMark()
            res = false
        }
        
        return res
    }
    fileprivate func validateAll() -> Bool {
        // Validate email
        
        // Validate password
        if (!Validation.isValidPhone(passwordTextField.text!)) {
            AlertUtility.showErrorAlert("Invalid Phone")
            return false
        }
        
        return true
    }
    
}

// =====================================
// ==== UITextFieldDelegate Methods ====
// =====================================



