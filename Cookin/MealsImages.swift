//
//  MealsImages.swift
//  Cookin
//
//  Created by Yo7ia on 4/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

//
//  MealsImages.swift
//  Cookin
//
//  Created by Yo7ia on 4/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import UIKit

class MealsImagesCell: UICollectionViewCell {
    
    @IBOutlet  weak var mealImage: UIImageView!
    
    fileprivate var _service:String? //TODO review
    var service: String {
        set {
            _service = newValue
            mealImage.sd_setImage(with: URL(string: _service!), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        get {
            return _service!
        }
    }
    
   
   
}
