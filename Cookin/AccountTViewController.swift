//
//  AccountTViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import Foundation
import UIKit
class AccountTViewController: BaseTableViewController{
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 1:
            performSegue(withIdentifier: "addphone", sender: nil)
            break
        case 2:
            performSegue(withIdentifier: "addemail", sender: nil)
            break
        case 3:
            performSegue(withIdentifier: "changepassword", sender: nil)
            break
        
        default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            segue.ccmPopUp(width: Int(Constants.ScreenWidth - 20), height: 220)
            segue.destination.view.AddBorderWithColor(radius: 15, borderWidth: 4, color: "01B0F0")
        switch segue.identifier! {
        case "addphone":
            let vc = segue.destination as! AddPhoneViewController
            vc.view.layer.cornerRadius = 15
            vc.successfullCompletionHandler = {
                
                vc.dismiss(animated: true, completion: nil)
                self.perform(#selector(self.showSuccessMSGPhone), with:self, afterDelay: 0.5)
            }
            break
        case "addemail":
            let vc = segue.destination as! AddEmailViewController
            vc.view.layer.cornerRadius = 15
            vc.successfullCompletionHandler = {
                
                vc.dismiss(animated: true, completion: nil)
                self.perform(#selector(self.showSuccessMSGEmail), with:self, afterDelay: 0.5)
            }
            break
        case "changepassword":
            let vc = segue.destination as! ChangePasswordVC
            vc.view.layer.cornerRadius = 15
            vc.successfullCompletionHandler = {
                
                vc.dismiss(animated: true, completion: nil)
                
                self.perform(#selector(self.showSuccessMSGPassword), with:self, afterDelay: 0.5)
            }
            break
        default:
            break
        }
    }
    func showSuccessMSGPassword()
    {
        AlertUtility.showSuccessAlert("Password Changed Successful")
    }
    
    func showSuccessMSGPhone()
    {
        AlertUtility.showSuccessAlert("Phone Added Successful")
    }
    func showSuccessMSGEmail()
    {
        AlertUtility.showSuccessAlert("Email Added Successful")
    }
}
