//
//  LoginViewController.swift
//  Phunation
//
//  Created by AMIT Developer on 10/19/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import UIKit


class LoginViewController : BaseViewController {
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var SignInBtn: UIButton!
    
    //MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SignInBtn.Curvyimage()
        mobileTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChangePassword(_:)), for: UIControlEvents.editingChanged)
    }
    
    //MARK: Actions
    @IBAction func togglePasswordSecureState() {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        
        //        if passwordTextField.isFirstResponder() {
        //            passwordTextField.becomeFirstResponder()
        //        }
    }
    
    @IBAction func loginButton() {
        if (!self.checkForRequiredFields()) {
            AlertUtility.showErrorAlert("Enter Mobile Number and password")
            
            return
        }
        
        if (!self.validateAll()) {
            return
        }
        UserModel.SignIn(data: ["MobileNumber":"1\(mobileTextField.text!)" as AnyObject,"Password":passwordTextField.text! as AnyObject]) { (user, err, errCode) in
            
            if !super.handleNetworkError(user ,msg: err, errorCode: 0 ,username: "") {
                //LOGIC
                let dic = (user as! [String: AnyObject])
                var data  = dic["client"] as! [String: AnyObject]
                data.updateValue(dic["Token"]! , forKey: "Token")
                data.updateValue("" as AnyObject, forKey: "username")
                data.updateValue("" as AnyObject, forKey: "City")

                UserModel.setUserEmail("1\(data["MobileNumber"] as! String)")
                UserModel.setUserPassword(data["password"] as! String)
                UserModel.setUserToken(dic["Token"] as! String)
                UserModel.setUserData(data)
                LoggedUser =  User(JSON: data )
                print("success")
                self.navigateToHome()
            }
        }
    }
    
    
    
    //MARK: private functions
    
    fileprivate func navigateToHome() {
        self.performSegue(withIdentifier: "goToHome", sender: nil)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
        } else {
            //            textField.hideExclamationMark()
            
            
        }
    }
    func textFieldDidChangePassword(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
//            showPasswordBtn.isHidden = true
        } else {
            //            textField.hideExclamationMark()
//            showPasswordBtn.isHidden = false
            
            
        }
    }
    override func SetupView() {
        self.navigationBarStyle = "default"
        super.SetupView()
    
    }
    
    
    
    
    //MARK: public functions
    
    
    //MARK: Validator
    fileprivate func checkForRequiredFields() -> Bool {
        var res = true
        
        // Check for email
        if (mobileTextField.text?.isEmpty == true) {
            //            userNameTextField.showExclamationMark()
            res = false
        }
        
        // Check for password
        if (passwordTextField.text?.isEmpty == true) {
            //            passwordTextField.showExclamationMark()
            res = false
        }
        
        return res
    }
    fileprivate func validateAll() -> Bool {
        // Validate email
        if  !Validation.isValidPhone(mobileTextField.text!){
            AlertUtility.showErrorAlert("Invalid Phone")
            return false
        }
        
        // Validate password
        if (!Validation.isValidPassword(passwordTextField.text!)) {
            AlertUtility.showErrorAlert("Minimum password length is 8 characters")
            return false
        }
        
        return true
    }
    
}

// =====================================
// ==== UITextFieldDelegate Methods ====
// =====================================



