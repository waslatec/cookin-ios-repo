//
//  AddPhoneViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit


class AddPhoneViewController: BaseViewController {
    
    
    @IBOutlet weak var currentPasswordTF: UITextField!
    var successfullCompletionHandler: ((Void) -> Void)?
    
    var userMgr = UserModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    
    
    @IBAction func dismiss() {
        self.dismissAnimated()
    }
    
    
    
    
    
    fileprivate func validateAll() -> Bool
        // Validate password
    {
        if (!Validation.isValidPhone(currentPasswordTF.text!)) {
            AlertUtility.showErrorAlert("Invalid Phone")
            return false
        }
        
        
        return true
    }
    
    
    @IBAction func changePassword() {
        self.view.endEditing(true)
        
        
        
        if (!self.validateAll()) {
            return
        }
        let params:[String: AnyObject] = ["MobileNumber": currentPasswordTF.text! as AnyObject]
        UserModel.ChangePassword(data: params) { (user, err, errCode) in
            
            if !super.handleNetworkError(user ,msg: err, errorCode: 0,username: "") {
                //LOGIC
                //                ChangePasswordVC.dismissAnimated()
                self.successfullCompletionHandler?()
                
            }
        }
        
    }
}


// =====================================
// ==== UITextFieldDelegate Methods ====
// =====================================

//extension ChangePasswordVC: UITextFieldDelegate {
//
//    override func textFieldShouldReturn(textField: UITextField) -> Bool {
//        switch textField {
//        case currentPasswordTF:
//            newPasswordTF.becomeFirstResponder()
//        case newPasswordTF:
//            changePassword()
//        default: break
//        }
//
//        return true
//    }
//    
//}
