//
//  HeaderCheifProfileCell.swift
//  Cookin
//
//  Created by Yo7ia on 4/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import UIKit
import FloatRatingView
protocol HeaderCheifProfileCellDelagete {
    func ShowLikes(_ cell: HeaderCheifProfileCell)
    func ShowProfile(_ cell: HeaderCheifProfileCell)
    func ShowNewMeal(_ cell: HeaderCheifProfileCell)
    func ShowPerformance(_ cell: HeaderCheifProfileCell)
    func ShowSetting(_ cell: HeaderCheifProfileCell)

}
class HeaderCheifProfileCell: UICollectionViewCell {
    
    @IBOutlet  weak var ProfileBanner: UIImageView!
    @IBOutlet  weak var ProfileLogo: UIImageView!
    @IBOutlet  weak var ProfileName: UILabel!
    @IBOutlet  weak var ProfileSettings: UIButton!
    @IBOutlet  weak var ProfileLikes: UIButton!
    @IBOutlet  weak var ProfileNew: UIButton!
    @IBOutlet  weak var ProfilePerformance: UIButton!
    @IBOutlet  weak var ProfileEdit: UIButton!

    @IBOutlet fileprivate weak var ProfileFollowings: UILabel!
    @IBOutlet fileprivate weak var ProfileFollowers: UILabel!
    @IBOutlet fileprivate weak var ProfileRating: FloatRatingView!

    
    var delegate: HeaderCheifProfileCellDelagete?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    fileprivate var _service:UserProfile? //TODO review
    
    var service: UserProfile {
        set {
            _service = newValue
            if LoggedUserProfile != nil
            {
                ProfileBanner.sd_setImage(with: URL(string: LoggedUserProfile!.CoverPhotoPath), placeholderImage: #imageLiteral(resourceName: "Cookin_Icons_32"))
                ProfileLogo.sd_setImage(with: URL(string: LoggedUserProfile!.ProfilePhotoPath), placeholderImage: #imageLiteral(resourceName: "Cookin_Icons_32"))
                ProfileName.text = LoggedUserProfile!.UserName
                ProfileRating.rating = Float(LoggedUserProfile!.Rate)
            }
            ProfileNew.AddBorderWithColor(radius: 5, borderWidth: 2, color: "01B0F0")
            ProfilePerformance.AddBorderWithColor(radius: 5, borderWidth: 2, color: "01B0F0")
            ProfileLikes.AddBorderWithColor(radius: 5, borderWidth: 2, color: "01B0F0")
            ProfileEdit.AddBorderWithColor(radius: 5, borderWidth: 2, color: "01B0F0")

        }
        
        get {
            return _service!
        }
    }
    @IBAction func ShowPerformance(_ sender: UIButton) {
        self.delegate?.ShowPerformance(self)
    }
    @IBAction func ShowNew(_ sender: UIButton) {
        self.delegate?.ShowNewMeal(self)
    }
    @IBAction func ShowProfile(_ sender: UIButton) {
        self.delegate?.ShowProfile(self)
    }
    @IBAction func ShowLikes(_ sender: UIButton) {
        self.delegate?.ShowLikes(self)
    }
    @IBAction func ShowSettings(_ sender: UIButton) {
        self.delegate?.ShowSetting(self)
    }
   
    
}
