//
//  DynamicContentBaseViewController.swift
//  Phunation
//
//  Created by AMIT Developer on 10/31/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit

class DynamicContentBaseViewController: BaseViewController {
    
    var data: [AnyObject]? = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
    }
    
     func loadData() {
        
    }
}