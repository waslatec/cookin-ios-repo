//
//  CheifMealsContainerViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/5/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
import UIKit
class CheifMealsContainerViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MealsViewController
        vc.loadMode = .CheifMeals
    }
}
