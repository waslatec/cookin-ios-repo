//
//  FirstView.swift
//  Cookin
//
//  Created by Yo7ia on 4/2/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import UIKit

class FirstView: BaseViewController {
    
        override func viewDidLoad() {
        super.viewDidLoad()
        let nc = self.storyboard!.instantiateViewController(withIdentifier: "OnBoarding")
        // Do any additional setup after loading the view, typically from a nib.
            
            
            let userMgr = UserModel()
            userMgr.checkIfLoggedIn(
                { () -> Void in
                    LoggedUser = userMgr.userData()
                    var homeVC :UITabBarController!
                    homeVC = self.storyboard!.instantiateViewController(withIdentifier: "HomeTabBarVC") as! UITabBarController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = homeVC
                    
            }, failureBlock: { (error) -> Void in
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = nc
            }
            )
            
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

