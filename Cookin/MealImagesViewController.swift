//
//  MealImagesViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import JTSImageViewController


class MealImagesViewController: DynamicCollectionBaseViewController {
    
    
    let reuseIdentifier = "MealsImagesCell"
    var meal: Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
    }
    override func loadData()
    {
        self.data = meal?.Images as [AnyObject]!
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
        
    override func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MealsImagesCell
        let shadowPath = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        cell.layer.shadowOpacity = 0.35
        cell.layer.shadowPath = shadowPath.cgPath
        //        layer.shouldRasterize = true
        cell.layer.cornerRadius = 3.0
        cell.service = self.data?[indexPath.row] as! String
        return cell
    }
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MealsImagesCell
        let imageView = cell.mealImage!
        let imageInfo = JTSImageInfo()
        imageInfo.image = imageView.image
        imageInfo.referenceRect = imageView.frame
        imageInfo.referenceView = imageView.superview
        
        let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: .image, backgroundStyle: .blurred)
        imageViewer?.show(from: self, transition: .fromOriginalPosition)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constants.ScreenWidth, height: 170);
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    
    
    
    
}
