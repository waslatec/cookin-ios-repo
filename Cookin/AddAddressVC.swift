//
//  AddAddressVC.swift
//  Washer-Client
//
//  Created by Amr on 2/20/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit

class AddAddressVC: BaseViewController {

    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var addressText: UITextField!
//    @IBOutlet weak var addressHint: UITextView!
    @IBOutlet weak var backButtonUI: UIButton!
    
    var map : MapVC?
    var address : ((lookupAddress) -> ())?
    var userAddrees : lookupAddress?
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonUI.layer.cornerRadius = 5
        addAddressButton.layer.cornerRadius = 5
        self.view.endEditing(true)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addAddress(_ sender: AnyObject) {
//        let DeliveryAddress = self.userAddrees?.formateer
////        let AddressHint = addressHint.text
//        let latt = self.userAddrees?.lat
//        let lng = self.userAddrees?.lng
        address?(self.userAddrees!)
        
        
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func backButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)

    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       map =  (segue.destination as! MapVC)
        
        map?.userAddress = { address in
             MapTasks.geocodeAddresswithlatlng(lat: address.lat!, lng: address.lng!, completion: { (lookupAddressResults) in
                if (lookupAddressResults.resultsAdd?.count)! > 0
                {
                self.addressText.text = lookupAddressResults.resultsAdd?[0].formateer
                self.userAddrees = lookupAddressResults.resultsAdd?[0]
                }
            })
            }
        
    }
        
   
 

}
