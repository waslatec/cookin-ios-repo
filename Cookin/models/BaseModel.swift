//
//  BaseModel.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

class BaseModel : NSObject{
    static let REST_NETWORK_MANAGER = "test"
    static let SUCCESS_ERROR_CODE = 0
    class func getErrorCode(_ data: AnyObject) -> Int {
        return SUCCESS_ERROR_CODE
    }
}
