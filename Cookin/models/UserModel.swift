
//
//  UserModel.swift
//  Phunation
//
//  Created by AMIT Developer on 10/24/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import FirebaseMessaging
import FirebaseInstanceID
import MapKit

var LoggedUser: User? {
didSet {
    
}
}
var LoggedUserProfile: UserProfile? {
didSet {
    
}
}
var CurrentUserLocation: CLLocation? {
    didSet {
        
    }
}
class UserModel: BaseModel {
    
    
    
    fileprivate static let USER_TOKEN_KEY = "USER_TOKEN_KEY"
    static var providerNow = true
    
    override init() {
        super.init()
        
        UserDefaultsAPI.setDefaults()
        
    }
    
    func SetFilterStates(data: [String: Bool])
    {
        UserDefaultsAPI.setUserPreference(data as AnyObject, key: "FilterStates")
    }
    func GetFilterStates() -> [String: Bool]
    {
        if UserDefaults.standard.object(forKey: "FilterStates") != nil
        {
        return UserDefaultsAPI.getUserPreference("FilterStates") as! [String: Bool]
        }
        else
        {
            return [String: Bool]()
        }
    }
    
    func GeneratePayFortParamsForGettingTokenForPayment() -> [String: AnyObject]{
        let RequestPhrase = Constants.PaymentConstants.Development.TestShain
        let Access_Code = Constants.PaymentConstants.Development.AccessCode
        let Language = Constants.PaymentConstants.Development.Language
        let Merchant = Constants.PaymentConstants.Development.MerchantIdentifier
        let Command = Constants.PaymentConstants.Development.ServiceCommand
        let DeviceID = Constants.DeviceID
        let Signature =
            "\(RequestPhrase)access_code=\(Access_Code)device_id=\(DeviceID)language=\(Language)merchant_identifier=\(Merchant)service_command=\(Command)\(RequestPhrase)".sha256()
        var Params = [String:AnyObject]()
        Params.updateValue(Command as AnyObject, forKey: "service_command")
        Params.updateValue(Access_Code as AnyObject, forKey: "access_code")
        Params.updateValue(DeviceID as AnyObject, forKey: "device_id")
        Params.updateValue(Merchant as AnyObject, forKey: "merchant_identifier")
        Params.updateValue(Language as AnyObject, forKey: "language")
        Params.updateValue(Signature as AnyObject, forKey: "signature")

        return Params
    }
    
    func checkProviderStatus() -> Bool{
        
        return UserModel.providerNow
       
    }
    func SwitchProviderStatus() {
        
        UserModel.providerNow = !UserModel.providerNow
    }
    func SwitchtoUserStatus() {
        
        UserModel.providerNow = false
    }
    func SwitchtoProviderStatus() {
        
        UserModel.providerNow = true
    }
    
    // ========================
    // ==== Getter Methods ====
    // ========================
    
    func firstTime() -> Bool {
        return UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.FirstTime) as? Bool ?? true
    }
    
    func userEmail() -> String {
        return UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.Email) as? String ?? ""
    }
    func userData() -> User {
        let xx = UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.UserData) as! [String : AnyObject]
        return User(JSON: xx)!
    }
    func getUserData() -> [String: AnyObject]
    {
        return UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.UserData) as! [String : AnyObject]

    }
    func userToken() -> String {
        return UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.Token) as? String ?? ""
    }

    func userPassword() -> String {
        return UserDefaultsAPI.getUserPreference(Constants.UserDefaultsConstants.Password) as? String ?? ""
    }
    
    
    // ========================
    // ==== Setter Methods ====
    // ========================
    
    func setFirstTime(_ firstTime: Bool!) {
        UserDefaultsAPI.setUserPreference(firstTime as AnyObject, key: Constants.UserDefaultsConstants.FirstTime)
    }
    
    
    
    class func setUserEmail(_ email: String!) {
        UserDefaultsAPI.setUserPreference(email as AnyObject, key: Constants.UserDefaultsConstants.Email)
    }
    class func setUserData(_ data: [String : AnyObject]!) {
        UserDefaultsAPI.setUserPreference(data as AnyObject, key: Constants.UserDefaultsConstants.UserData)
    }
    class func setUserToken(_ email: String!) {
        UserDefaultsAPI.setUserPreference(email as AnyObject, key: Constants.UserDefaultsConstants.Token)
    }

    class func setUserPassword(_ password: String!) {
        UserDefaultsAPI.setUserPreference(password as AnyObject, key: Constants.UserDefaultsConstants.Password)
    }
    
    // Signin Method
    
    class func SignIn(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.Login) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    
    // Register Method

    class func Register(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.Signup) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // ForgetPassword Method
    
    class func ForgetPassword(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.ForgetPassword) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // ConfirmCode Method
    
    class func ConfirmCode(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.ConfirmCode) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // ChangePassword

    class func ChangePassword(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.UpdatePassword) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // ChangeEmail
    
    class func ChangeEmail(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        
        RequestPostAPI(data: data, url: Constants.APIProvider.UpdateEmail) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    // GetProfile
    class func GetProfile(callback: @escaping (AnyObject?, String?, Int) -> Void) {
        let data = [String: AnyObject]()
        RequestPostAPI(data: data, url: Constants.APIProvider.GetProfile) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // SearchMeals
    class func SearchMeals(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        let data = [String: AnyObject]()
        RequestPostAPI(data: ["MapLat": 30.073365 as AnyObject,"MapLng": 31.317533 as AnyObject], url: Constants.APIProvider.SearchMeals) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // GetCheifMeals
    class func GetCheifMeals(data: [String: AnyObject] , callback: @escaping (AnyObject?, String?, Int) -> Void) {
        let data = [String: AnyObject]()
        RequestPostAPI(data: data, url: Constants.APIProvider.GetCheifMeals) { (result, error, errCode) in
            callback(result, error, errCode)
        }
    }
    
    // UpdateProfile
    class func UpdateProfile(NickName: String,UserName: String, Password: String,PhoneNumber : String ,Email: String ,BirthDate: String ,MapLat: Double ,MapLng: Double ,CityId: Int ,isCheef: Bool, LogoPath: Data? , BannerPath : Data?, callback: @escaping (AnyObject?, String?, Int) -> Void) {
        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
        mangar?.doPostMultiPart(NickName: NickName,UserName: UserName, Password: Password,PhoneNumber : PhoneNumber ,Email: Email ,BirthDate: BirthDate ,MapLat: MapLat ,MapLng: MapLng ,CityId: CityId ,isCheef: isCheef, LogoPath: LogoPath , BannerPath : BannerPath, url: Constants.APIProvider.UpdateProfile, callback: { (result: AnyObject, error: String?, errCode: Int) in
            if error != nil {
                callback(result, error, errCode)
                
            } else {
                //Content error
                let contentErrCode = getErrorCode(result)
                if contentErrCode != SUCCESS_ERROR_CODE {
                    callback(result, error, contentErrCode)
                    
                    //Success
                } else {
                    
                    callback(result, nil, SUCCESS_ERROR_CODE)
                }
            }
            
        })
    }
    
    // AddMeal
    class func AddMeal(MealName: String,MealDescription: String, MealPrice: String,MealUnitNumber : String ,MealTypeID: Int ,ClientId: String ,Weight: Double ,isHeat: Bool, Image1: Data? , Image2 : Data?, Image3 : Data?, Image4 : Data?, callback: @escaping (AnyObject?, String?, Int) -> Void) {
        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
        mangar?.doPostAddMeal(MealName: MealName,MealDescription: MealDescription, MealPrice: MealPrice,MealUnitNumber : MealUnitNumber ,MealTypeID: MealTypeID ,ClientId: ClientId ,Weight: Weight ,isHeat: isHeat, Image1: Image1 , Image2 : Image2, Image3 : Image3, Image4 : Image4, url: Constants.APIProvider.AddMeal, callback: { (result: AnyObject, error: String?, errCode: Int) in
            if error != nil {
                callback(result, error, errCode)
                
            } else {
                //Content error
                let contentErrCode = getErrorCode(result)
                if contentErrCode != SUCCESS_ERROR_CODE {
                    callback(result, error, contentErrCode)
                    
                    //Success
                } else {
                    
                    callback(result, nil, SUCCESS_ERROR_CODE)
                }
            }
            
        })
    }
    class func RequestPostAPI(data: [String: AnyObject],url: String , callback: @escaping (AnyObject?, String?, Int) -> Void)
    {
        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
        mangar?.doPost(url,
                       params: data, CheckProgress: true,
                       callback: { (result: AnyObject, error: String?, errCode: Int) in
                        
                        //Network error
                        if error != nil {
                            callback(result, error, errCode)
                            
                        } else {
                            //Content error
                            let contentErrCode = getErrorCode(result)
                            if contentErrCode != SUCCESS_ERROR_CODE {
                                callback(result, error, contentErrCode)
                                
                                //Success
                            } else {
                                callback(result, nil, SUCCESS_ERROR_CODE)
                            }
                        }
        })
}
//
//    
//    // ChangePassword Method
//    
//    class func changePassword(_ password: String, newpassword: String , callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.ChangePassword,
//                       params: ["token": LoggedUser!.token as AnyObject, "old_password": password as AnyObject , "new_password": newpassword as AnyObject ], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                       
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                            //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    
//    
//    // SendReview Method
//    
//    class func SendReview(_ serviceID: String, title: String ,review: String , rate: Float , callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.Reviews,
//                       params: ["token": LoggedUser!.token as AnyObject, "service_id": serviceID as AnyObject , "review": review as AnyObject ,"title": title as AnyObject ,"rate": rate as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    
//    
//    ///getting Transaction
//    class func getTransaction(callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.tranaction,
//                       params: ["token": LoggedUser!.token as AnyObject], CheckProgress: false,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    
//   
//    
//    // Reset Password
//    class func ResendActivation(_ email: String, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.ResendActivation,
//                       params: ["email": email as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    
//    // Reset Password
//    class func ResetPassword(_ email: String, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.ResetPassword,
//                       params: ["email": email as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    
//    
//    // Register Push Notification
//    class func RegisterFCM(_ token: String, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        let  params = ["token": LoggedUser!.token,"push_token": token , "type": "apple"]
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.RegisterNotification,
//                       params: ["token": LoggedUser!.token as AnyObject,"push_token": token as AnyObject , "type": "apple" as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    // Check-Availibility Method
//    class func CheckAvailibility(params: [String: AnyObject], callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.CheckAvailibilty,
//                       params: params, CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    class func CancelRequestUser(request_id: Int, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.CancelRequest,
//                       params: ["token": LoggedUser!.token as AnyObject , "request_id": request_id as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//
//    class func ApproveOrRejectRequestProvider(request_id: Int,status: Int, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.ApproveOrReject,
//                       params: ["token": LoggedUser!.token as AnyObject , "request_id": request_id as AnyObject , "status": status as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//
//    // signIn Method
//    class func siginIn(_ userName: String, password: String , callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.Login,
//                       params: ["parameter": userName as AnyObject, "password": password as AnyObject], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                let dic = result as! [String : AnyObject]
//                                callback(dic as AnyObject, nil, SUCCESS_ERROR_CODE)
//                                // [END subscribe_topic]
//
//                            }
//                        }
//        })
//    }

//    
//    func isLoggedIn() -> Bool
//    {
//        checkIfLoggedIn(
//            { () -> Void in
//                return true
//                
//            }, failureBlock: { (error) -> Void in
//               return false
//        })
//        return false
//    }
    func checkIfLoggedIn(_ successBlock: @escaping () -> Void, failureBlock: @escaping (_ error: String) -> Void) {
        let email: String! = userEmail()
        let password: String! = userPassword()
        if (email != "" && password != "")
        {
        UserModel.SignIn(data: ["MobileNumber": email as AnyObject,"Password":password! as AnyObject]) { (user, err, errCode) in
            
            if (err == nil &&  errCode == BaseModel.SUCCESS_ERROR_CODE) {
                let dic = (user as! [String: AnyObject])
                var data  = dic["client"] as! [String: AnyObject]
                data.updateValue(dic["Token"]! , forKey: "Token")
                data.updateValue("" as AnyObject, forKey: "username")
                data.updateValue("" as AnyObject, forKey: "City")
                
                UserModel.setUserEmail("1\(data["MobileNumber"] as! String)")
                UserModel.setUserPassword(data["password"] as! String)
                UserModel.setUserToken(dic["Token"] as! String)
                UserModel.setUserData(data)
                LoggedUser =  User(JSON: data )
                print("success")
                successBlock()

            }
            else
            {
                failureBlock(NSLocalizedString("Guest User", comment: ""))
  
            }
            
        }
        }
        else
        {
            failureBlock(NSLocalizedString("Guest User", comment: ""))
        }
            
       }
    class func setLoggedIn(_ token: String?) {
        UserDefaults.standard.set(token, forKey: USER_TOKEN_KEY)
    }
    
    class func logout()
    {
        setUserData([String: AnyObject]())
        setUserEmail("")
        setUserToken("")
        setUserPassword("")
        LoggedUser = nil
        LoggedUserProfile = nil
        

    }
    
     func isLoggedIn() -> Bool {
       let token =  userEmail()
        return token == "" ? false : true
    }
    
//    class func siginUp(_ userName: String, password: String ,email: String,phone: String, fullName: String, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.Signup,
//                       params: ["username": userName as AnyObject, "password": password as AnyObject ,"name": fullName as AnyObject , "email": email as AnyObject,"phone": phone as AnyObject ], CheckProgress: true,
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                    // [START get_iid_token]
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
    
//    class func validateProfile(_ email: String,phone: String, fullName: String,birthdate : AnyObject, callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPost(Constants.APIProvider.ValidateProfile,
//                       params: ["token": LoggedUser!.token as AnyObject,"email": email as AnyObject, "name": fullName as AnyObject ,"telephone": phone as AnyObject as AnyObject ,"birthday": birthdate],
//                       callback: { (result: AnyObject, error: String?, errCode: Int) in
//                        
//                        //Network error
//                        if error != nil {
//                            callback(result, error, errCode)
//                            
//                        } else {
//                            //Content error
//                            let contentErrCode = getErrorCode(result)
//                            if contentErrCode != SUCCESS_ERROR_CODE {
//                                callback(result, error, contentErrCode)
//                                
//                                //Success
//                            } else {
//                                
//                                callback(result, nil, SUCCESS_ERROR_CODE)
//                            }
//                        }
//        })
//    }
//    class func UpdateProfile(_ email: String,phone: String, fullName: String,birthdate : AnyObject,gender: String , LogoPath: Data?, BannerPath: Data? , callback: @escaping (AnyObject?, String?, Int) -> Void) {
//        ProgressUtility.showProgressView()
//        let mangar = NetworkManagerFactory.createNetworkManager(REST_NETWORK_MANAGER)
//        mangar?.doPostMultiPart(email: email, phone: phone.replacingOccurrences(of: "+ ", with: ""), fullName: fullName, birthdate: birthdate as! NSNumber, gender: gender, LogoPath: LogoPath, BannerPath: BannerPath, url: Constants.APIProvider.UpdateProfile, callback: { (result: AnyObject, error: String?, errCode: Int) in
//            if error != nil {
//                callback(result, error, errCode)
//                
//            } else {
//                //Content error
//                let contentErrCode = getErrorCode(result)
//                if contentErrCode != SUCCESS_ERROR_CODE {
//                    callback(result, error, contentErrCode)
//                    
//                    //Success
//                } else {
//                    
//                    callback(result, nil, SUCCESS_ERROR_CODE)
//                }
//            }
//
//        })
//        
//        
////        let headers = []
////        let parameters = [
////            [
////                "name": "email",
////                "value": "taa@mailinator.com"
////            ],
////            [
////                "name": "token",
////                "value": "c578d3fb06ae048dd4055c159997dfd3436563beb0fe611c0e099fe9ce438d2cf7b2c1f9dbd89dccf24f2e679be9e070"
////            ],
////            [
////                "name": "name",
////                "value": "ahmedadsw"
////            ],
////            [
////                "name": "telephone",
////                "value": "123456789"
////            ],
////            [
////                "name": "gender",
////                "value": "m"
////            ],
////            [
////                "name": "birthdate",
////                "value": "0"
////            ],
////            [
////                "name": "logo",
////                "fileName": LogoPath?.path ?? "",
////                "content-type": "image/png"
////
////            ],
////            [
////                "name": "phone",
////                "value": "013131412"
////            ],
////            [
////                "name": "banner",
////                "fileName": BannerPath?.path ?? "",
////                "content-type": "image/png"
////            ]
////        ]
////        
////        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
////        
////        var body : NSMutableData?
////        var error: NSError? = nil
////        for param in parameters {
////            let paramName = param["name"]!
////            body?.append("--\(boundary)\r\n".data(using: .utf8)!)
////            body?.append("Content-Disposition:form-data; name=\"\(paramName)\"".data(using: .utf8)!)
////            if let filename = param["fileName"] {
////                let contentType = param["content-type"]!
////
//////                let path1 = Bundle.main.path(forResource: "placeholder", ofType: "png")!
////                if(filename != "")
////                {
////               let x =  UIImage(contentsOfFile: filename)
//////                let url = URL(fileURLWithPath: path1)
//////                let filename = url.lastPathComponent
////                
////                
////                do {
////                let data = x!.jpeg(.highest)
//////                let fileContent = try String(contentsOfFile: filename, encoding: .utf8)
//////                if (error != nil) {
//////                    print(error)
//////                }
////                body?.append("; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
////                body?.append("Content-Type: \(contentType)\r\n\r\n".data(using: .utf8)!)
////                body?.append(data!)
////                }
////                catch {
////                    print(error.localizedDescription)
////                }
////                }
////            } else if let paramValue = param["value"] {
////                body?.append("\r\n\r\n\(paramValue)".data(using: .utf8)!)
////            }
////        }
////        
////        let request = NSMutableURLRequest(url: NSURL(string: "http://52.42.186.30/phonation_test/dev/api/users/profile")! as URL,
////                                          cachePolicy: .useProtocolCachePolicy,
////                                          timeoutInterval: 10.0)
////        request.httpMethod = "POST"
////        request.allHTTPHeaderFields = headers
////        
////        request.httpBody = body as Data?
////        
////        let session = URLSession.shared
////        
////        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
////            ProgressUtility.dismissProgressView()
////            if (error != nil) {
////                print(error)
////            } else {
////                let httpResponse = response as? HTTPURLResponse
////                print(httpResponse)
////            }
////        })
////        
////        dataTask.resume()
//    }
//    
}
