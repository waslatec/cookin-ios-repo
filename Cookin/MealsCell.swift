//
//  MealsCell.swift
//  Cookin
//
//  Created by Yo7ia on 4/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import UIKit
protocol MealsCellDelagete {
    func ShowMealImages(_ cell: MealsCell)
    func ShareMeal(_ cell: MealsCell)

}
class MealsCell: UICollectionViewCell {
    
    @IBOutlet  weak var mealImage: UIImageView!
    @IBOutlet fileprivate weak var mealTitle: UILabel!
    @IBOutlet  weak var mealDescription: UILabel?
    @IBOutlet fileprivate weak var mealPrice: UILabel!
    @IBOutlet fileprivate weak var mealDistance: UILabel!
    @IBOutlet fileprivate weak var mealCount: UILabel!
    @IBOutlet fileprivate weak var mealShowImages: UIImageView!
    @IBOutlet fileprivate weak var mealShare: UIImageView!

    var delegate: MealsCellDelagete?
   
    
    fileprivate var _service:Meal? //TODO review
    var service: Meal {
        set {
            _service = newValue
            
            let singleTap = UITapGestureRecognizer(target: self,action: #selector(MealsCell.ShowImages))
            singleTap.numberOfTapsRequired = 1
            self.mealShowImages.isUserInteractionEnabled = true
            self.mealShowImages.addGestureRecognizer(singleTap)
            let singleTapShare = UITapGestureRecognizer(target: self,action: #selector(MealsCell.ShareMeal))
            singleTapShare.numberOfTapsRequired = 1
            self.mealShare.isUserInteractionEnabled = true
            self.mealShare.addGestureRecognizer(singleTapShare)
            if (_service?.Images.count)! > 0
            {
                self.mealImage.sd_setImage(with: URL(string: (_service?.Images[0])!), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            }
            else
            {
                self.mealImage.image =  #imageLiteral(resourceName: "placeholder")

            }
            self.mealTitle.text = _service!.Name
            self.mealPrice.text = "SAR \(_service!.Price)"
            self.mealCount.text = "\(_service!.UnitNumber)"
            self.mealDescription?.text = _service!.MealDescription
            self.mealDistance.text = CurrentUserLocation?.DistanceBetweenTwoLocations(lat: _service!.MapLat,lng: _service!.MapLng)
        }
        
        get {
            return _service!
        }
    }
    func ShowImages()
    {
        self.delegate?.ShowMealImages(self)
    }
    func ShareMeal()
    {
        self.delegate?.ShareMeal(self)
    }

}
