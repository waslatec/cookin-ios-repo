//
//  MealDetailsTViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
import UIKit
import FloatRatingView
class MealDetailsTViewController: BaseTableViewController{
    
    @IBOutlet fileprivate weak var mealImage: UIImageView!
    @IBOutlet fileprivate weak var mealTitle: UILabel!
    @IBOutlet fileprivate weak var mealDescription: UILabel?
    @IBOutlet fileprivate weak var mealDistance: UILabel?
    @IBOutlet fileprivate weak var mealWeight: UILabel?
    @IBOutlet fileprivate weak var mealPersonNumber: UILabel?
    @IBOutlet fileprivate weak var mealPrice: UILabel!
    @IBOutlet fileprivate weak var mealRatingCount: UILabel!
    @IBOutlet fileprivate weak var mealRating: FloatRatingView!

    @IBOutlet fileprivate weak var mealShowImages: UIImageView!
    @IBOutlet fileprivate weak var mealShare: UIImageView!
    @IBOutlet fileprivate weak var mealAvaialble: UIImageView!
    @IBOutlet fileprivate weak var mealNutritionFacts: UIImageView!
    var meal: Meal?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let singleTap = UITapGestureRecognizer(target: self,action: #selector(ShowMealImages))
        singleTap.numberOfTapsRequired = 1
        self.mealShowImages.isUserInteractionEnabled = true
        self.mealShowImages.addGestureRecognizer(singleTap)
        let singleTapShare = UITapGestureRecognizer(target: self,action: #selector(ShareMeal))
        singleTapShare.numberOfTapsRequired = 1
        self.mealShare.isUserInteractionEnabled = true
        self.mealShare.addGestureRecognizer(singleTapShare)
        if (meal?.Images.count)! > 0
        {
        self.mealImage.sd_setImage(with: URL(string: meal!.Images[0]), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        self.mealTitle.text = meal!.Name
        self.mealPrice.text = "SAR \(meal!.Price)"
        self.mealPersonNumber!.text = "\(meal!.UnitNumber)"
        self.mealDescription!.text = meal!.MealDescription
        self.mealWeight!.text = "\(meal!.Weight) Kg"
        self.mealRating.rating = Float(meal!.Rate)
        self.mealAvaialble.image = meal!.IsAviliable ? #imageLiteral(resourceName: "Cookin_Icons_155") :#imageLiteral(resourceName: "Cookin_Icons_180")
        self.mealDistance!.text = CurrentUserLocation?.DistanceBetweenTwoLocations(lat: meal!.MapLat,lng: meal!.MapLng)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showmealsImages" {
            let vc = segue.destination as! MealImagesViewController
            vc.meal = self.meal
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row
        {
        case 0 :
            return 300
        case 1:
            return 95
        case 2:
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }

    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row
        {
        case 0 :
            return 300
        case 1:
            return 95
        case 2:
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }

    }
    func ShowMealImages() {
        performSegue(withIdentifier: "showmealsImages", sender: nil)
    }
    func ShareMeal() {
        self.navigationController?.shareImageAndText(image: mealImage.image!, text: mealDescription!.text!)
    }
    
}
