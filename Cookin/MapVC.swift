//
//  MapVC.swift
//  Washer-Client
//
//  Created by Amr on 1/29/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import GoogleMaps
import Foundation
import ObjectMapper
import Alamofire

class MapVC: BaseViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var mylocation: UITextField!
    
    @IBOutlet var viewMap: GMSMapView!
    var marker : GMSMarker?
    var testgecoding = MapTasks()
    var userAddress: ((lookupAddress) -> ())?
    var prevLocation : CLLocation?
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)

        if CLLocationManager.locationServicesEnabled()
            && CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
        locationManager.startUpdatingLocation()
            locationManager.delegate = self
            viewMap.delegate = self
//            viewMap.isMyLocationEnabled = true
            viewMap.settings.myLocationButton = true
           
        }
        }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.first
        if let location = location {
            
            if prevLocation == nil {
                prevLocation = location
                getLocation(location: prevLocation!)
            } else {
                if prevLocation!.distance(from: location) > 500 {
                    prevLocation = location
                    getLocation(location: prevLocation!)
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        getLocation(location: location)
    }
//    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
//        if prevLocation != nil {
//            getLocation(location: prevLocation!)
//        }
//    }
    
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if prevLocation != nil {
            marker?.map = nil
        getLocation(location: prevLocation!)
        }
        return true
    }
    func getLocation(location: CLLocation) {
        
        viewMap.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 16.0)
        
//        marker?.map = nil
//        marker = GMSMarker(position: location.coordinate)
//        marker?.icon = UIImage(named: "mapMarkerUser")
//        marker?.map = viewMap
        
        
//        let address = lookupAddress()
//        address.lat = location.coordinate.latitude
//        address.lng = location.coordinate.longitude
//        userAddress?(address)
    }
    
    func drawMarker(location: CLLocation,title: String) {
                marker?.map = nil
                marker = GMSMarker(position: location.coordinate)
                marker?.icon = UIImage(named: "driverPin")
                marker?.map = viewMap
                marker?.title = title
    }
    

    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {

        let coor = mapView.projection.coordinate(for: viewMap.center)
        let address = lookupAddress()
        address.lat = coor.latitude
        address.lng = coor.longitude
        userAddress?(address)
    

    
}
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
       
            let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving")!
            let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
            print(accessToken)
            Alamofire.request( url, method: .post , headers: headers).responseString { response in
                if let JSON = response.result.value {
                    if JSON != "null" {
                     let r = Mapper<dic>().map(JSONString: JSON)!
                         self.showPath(polyStr: (r.resultsAdd?[0].point!)!)
                    }else{
                        
                    }
                } else {
                    print("Error")
                }
            }
       
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = viewMap // Your map view
    }



}


class dic: Mappable {
    var resultsAdd : [ddc]?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultsAdd <- map["routes"]
        
    }
    
}
class ddc: Mappable {
    
    var point : String?
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        point <- map["overview_polyline.points"]
        
        
        
    }
}


