//
//  EditProfileTViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import AVFoundation
import UIKit
import FloatRatingView
import Photos
import LKAlertController
import MobileCoreServices


class EditProfileTViewController: BaseTableViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet fileprivate weak var ProfileBanner: UIImageView!
    @IBOutlet fileprivate weak var ProfileLogo: UIImageView!
    @IBOutlet fileprivate weak var ProfileName: UITextField!
    @IBOutlet fileprivate weak var ProfileBirthDate: UITextField!
    @IBOutlet fileprivate weak var ProfileCity: UITextField!
    @IBOutlet fileprivate weak var ProfileLocation: UITextField!

    @IBOutlet fileprivate weak var ProfileSaveBtn: UIButton!
    var userAddress: lookupAddress?
    var check = true
    var UpdateProfileIcon = true
    var bannerPath:Data?
    var LogoPath:Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserModel.GetProfile { (data, error, errCode) in
            if !super.handleNetworkError(data ,msg: error, errorCode: 0 ,username: "") {
                let dic = (data as! [String: AnyObject])
                LoggedUserProfile = UserProfile(JSON: dic)
                self.initUI()
            }
        }
      
       
    }
    
   func initUI()
    {
    ProfileSaveBtn.Curvyimage()
    let singleTap = UITapGestureRecognizer(target: self,action: #selector(EditBannerLogo))
    singleTap.numberOfTapsRequired = 1
    self.ProfileBanner.isUserInteractionEnabled = true
    self.ProfileBanner.addGestureRecognizer(singleTap)
    let singleTapShare = UITapGestureRecognizer(target: self,action: #selector(EditProfileLogo))
    singleTapShare.numberOfTapsRequired = 1
    self.ProfileLogo.isUserInteractionEnabled = true
    self.ProfileLogo.addGestureRecognizer(singleTapShare)
    self.ProfileBanner.sd_setImage(with: URL(string: LoggedUserProfile!.CoverPhotoPath), placeholderImage: #imageLiteral(resourceName: "Cookin_Icons_32"))
        self.ProfileLogo.sd_setImage(with: URL(string: LoggedUserProfile!.ProfilePhotoPath), placeholderImage: #imageLiteral(resourceName: "Cookin_Icons_32"), options: .progressiveDownload)
    self.ProfileLogo.sd_setImage(with: URL(string: LoggedUserProfile!.ProfilePhotoPath), placeholderImage: #imageLiteral(resourceName: "Cookin_Icons_32"),options: .progressiveDownload)
        ProfileName.text = LoggedUserProfile!.UserName
    MapTasks.geocodeAddresswithlatlng(lat: LoggedUserProfile!.MapLat, lng: LoggedUserProfile!.MapLng) { (lookupaddress) in
        if (lookupaddress.resultsAdd?.count)! > 0
        {
            self.ProfileLocation.text = lookupaddress.resultsAdd![0].formateer
        }
        }
    self.ProfileCity.text = LoggedUserProfile!.CityId
    self.ProfileBirthDate.text = LoggedUserProfile!.BirthDay
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "location"
        {
            segue.ccmPopUp(height: 500)
            let vc = segue.destination as! AddAddressVC
            vc.address = { address in
                self.userAddress = address
                self.ProfileLocation.text = address.formateer!
                LoggedUserProfile!.MapLat = address.lat!
                LoggedUserProfile!.MapLng = address.lng!
            }
        }
    }
    
    
    fileprivate func validateAll() -> Bool {
        // Validate email
        if (!Validation.isValidUsername(ProfileName.text!)  ) {
            AlertUtility.showErrorAlert("Enter Valid Nickname")
            return false
        }
        return true
    }

    
    @IBAction func dateTextInputPressed(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
//        if self.brithdayTextFiled.text! == ""
//        {
//            
//        }
//        else
//        {
//            datePickerView.date = Date(timeIntervalSince1970: self.brithdayTextFiled.text!.toDate.timeIntervalSince1970+7200)
//            
//        }
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 38))
        let done = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(super.dismissKeyboard))
        let clear = UIBarButtonItem(title: NSLocalizedString("Clear", comment: ""), style: .done, target: self, action: #selector(clearDate))
        
        toolBar.items = [
            clear,
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            done
        ]
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = toolBar
        
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    func datePickerValueChanged(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        //        self.DummyuserProfile?.birthdate = sender.date.timeIntervalSince1970+10000
        
        self.ProfileBirthDate.text = dateFormatter.string(from: sender.date)
    }
    func clearDate()
    {
        self.ProfileBirthDate.text = ""
        
    }
    
    @IBAction func locationBtnClicked(_ sender: Any) {
        performSegue(withIdentifier: "location", sender: nil)
    }
    
    @IBAction func EditProfileSaveBtnClicked(_ sender: Any) {
        if (!self.validateAll()) {
            return
        }
        UserModel.UpdateProfile(NickName: ProfileName.text!, UserName: LoggedUserProfile!.UserName, Password: LoggedUser!.password, PhoneNumber: LoggedUser!.MobileNumber, Email: LoggedUserProfile!.Email, BirthDate: ProfileBirthDate.text!, MapLat: LoggedUserProfile!.MapLat, MapLng: LoggedUserProfile!.MapLng, CityId: 1, isCheef: LoggedUserProfile!.isCheef, LogoPath: LogoPath, BannerPath: bannerPath) { (data, error, errCode) in
            if !super.handleNetworkError(data as AnyObject ,msg: error, errorCode: 0 ,username: "") {
            
                let datas = data as! NSDictionary

                AlertUtility.showSuccessAlert(datas["EnglishMessage"] as! String)
            }
        }
    }
    
    
    func EditBannerLogo() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.UpdateProfileIcon = false
                
                self?.showImagePickerWithSourceType(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.UpdateProfileIcon = false
                
                self?.showImagePickerWithSourceType(.camera)
            }
        }
        
        //            sheet.addAction("Delete", style: .destructive) { [weak self] _ in
        ////                self!.DummyuserProfile?.main_image = nil
        //                self!.profileBanner.image = #imageLiteral(resourceName: "placeholder")
        //
        //            }
        
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    func EditProfileLogo() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.UpdateProfileIcon = true
                self?.checkLibrary(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.UpdateProfileIcon = true
                self?.checkCamera(.camera)
                
            }
        }
       
        
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    
    
    fileprivate func showImagePickerWithSourceType(_ sourceType: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = sourceType;
        picker.mediaTypes = [kUTTypeImage as String]
        picker.allowsEditing = false
        
        present(picker, animated: true, completion: nil)
    }
    
    func checkCamera(_ sourceType: UIImagePickerControllerSourceType) {
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authStatus {
        case .authorized: showImagePickerWithSourceType(sourceType) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageCameraAccessInitially(sourceType)
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting(sourceType)
        default: alertToEncourageCameraAccessInitially(sourceType)
        }
    }
    func checkLibrary(_ sourceType: UIImagePickerControllerSourceType) {
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
        case .authorized: showImagePickerWithSourceType(sourceType) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageLibraryAccessInitially(sourceType)
        case .notDetermined: alertPromptToAllowLibraryAccessViaSetting(sourceType)
        default: alertToEncourageLibraryAccessInitially(sourceType)
        }
    }
    
    func alertToEncourageLibraryAccessInitially(_ sourceType: UIImagePickerControllerSourceType) {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Library access required for getting photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Library", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowLibraryAccessViaSetting(_ sourceType: UIImagePickerControllerSourceType) {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Library access required for getting photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            PHPhotoLibrary.requestAuthorization({ (status) in
                self.checkLibrary(.photoLibrary)
            })
            
        })
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func alertToEncourageCameraAccessInitially(_ sourceType: UIImagePickerControllerSourceType) {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting(_ sourceType: UIImagePickerControllerSourceType) {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        self.checkCamera(sourceType) } }
            }
            }
        )
        present(alert, animated: true, completion: nil)
    }
    
 
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        self.check = false
//        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
//        let fileURL = documentsDirectoryURL.appendingPathComponent("\(Date().timeIntervalSince1970).jpeg")
        
        let imgg = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        let Data = imgg.jpeg(.low)
        
        if self.UpdateProfileIcon {
            LogoPath = Data
            self.ProfileLogo.image = imgg
        }else
        {
            bannerPath = Data
            self.ProfileBanner.image = imgg
        }
        
        
    }
    
}
