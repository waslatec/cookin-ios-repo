//
//  SignUpViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/9/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit


class SignUpViewController : BaseViewController {
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var isChefSwitch: UISwitch!
    var userAddress: lookupAddress?

    @IBOutlet weak var SignUpBtn: UIButton!
    //MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SignUpBtn.Curvyimage()

        mobileTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChangePassword(_:)), for: UIControlEvents.editingChanged)
    }
    
    @IBAction func locationBtnClicked(_ sender: Any) {
        performSegue(withIdentifier: "location", sender: nil)
    }
    @IBAction func isChefBtnClicked(_ sender: Any) {
        isChefSwitch.setOn(!isChefSwitch.isOn, animated: true)
    }
    //MARK: Actions
    @IBAction func togglePasswordSecureState() {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        
        //        if passwordTextField.isFirstResponder() {
        //            passwordTextField.becomeFirstResponder()
        //        }
    }
    
    @IBAction func SignUpButtonClicked() {
        if (!self.checkForRequiredFields()) {
            AlertUtility.showErrorAlert("Fill All Fields To Register")
            
            return
        }
        
        if (!self.validateAll()) {
            return
        }
        let parameters = ["MapLat":userAddress!.lat!,"MapLng":userAddress!.lng!,"Name":emailTextField.text! ,"IsCheif":isChefSwitch.isOn ? "true" : "false","MobileNumber":mobileTextField.text!,"Email":emailTextField.text! ,"Password":passwordTextField.text!,"MobileTypeId":2] as [String : AnyObject]
        UserModel.Register(data: parameters) { (user, err, errCode) in
            
            if !super.handleNetworkError(user ,msg: err, errorCode: 0 ,username: "") {
                //LOGIC
                print("success")
                self.performSegue(withIdentifier: "activate", sender: nil)
            }
        }
    }
    
    
    
    //MARK: private functions
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "location"
        {
            segue.ccmPopUp(height: 500)
            let vc = segue.destination as! AddAddressVC
            vc.address = { address in
                self.userAddress = address
                self.locationBtn.setTitle(address.formateer!, for: .normal)
            }

        }
        else if segue.identifier == "activate"
        {
            let vc = segue.destination as! ConfirmViewController
            vc.mobile = mobileTextField.text!
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
        } else {
            //            textField.hideExclamationMark()
            
            
        }
    }
    func textFieldDidChangePassword(_ textField: UITextField) {
        if (textField.text?.isEmpty == true) {
            //            textField.showExclamationMark()
            //            showPasswordBtn.isHidden = true
        } else {
            //            textField.hideExclamationMark()
            //            showPasswordBtn.isHidden = false
            
            
        }
    }
    override func SetupView() {
        self.navigationBarStyle = "default"
        super.SetupView()
        
    }
    
    
    
    
    //MARK: public functions
    
    
    //MARK: Validator
    fileprivate func checkForRequiredFields() -> Bool {
        var res = true
        
        // Check for email
        if (mobileTextField.text?.isEmpty == true) {
            //            userNameTextField.showExclamationMark()
            res = false
        }
        
        // Check for password
        if (passwordTextField.text?.isEmpty == true) {
            //            passwordTextField.showExclamationMark()
            res = false
        }
        if (emailTextField.text?.isEmpty == true) {
            //            passwordTextField.showExclamationMark()
            res = false
        }
        if (userAddress == nil) {
            //            passwordTextField.showExclamationMark()
            res = false
        }
        return res
    }
    fileprivate func validateAll() -> Bool {
        // Validate phone
        if  !Validation.isValidPhone(mobileTextField.text!){
            AlertUtility.showErrorAlert("Invalid Phone")
            return false
        }
        
        // Validate email
        if (!Validation.isValidEmail(emailTextField.text!)) {
            AlertUtility.showErrorAlert("Enter Valid email  address \"Example@gmail.com\"")
            return false
        }
        
        // Validate password
        if (!Validation.isValidPassword(passwordTextField.text!)) {
            AlertUtility.showErrorAlert("Minimum password length is 5 characters")
            return false
        }
        
        return true
    }
    
}

// =====================================
// ==== UITextFieldDelegate Methods ====
// =====================================



