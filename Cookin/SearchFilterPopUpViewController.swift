//
//  SearchFilterPopUpViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit

protocol SaveFilterClickedDelegate {
    func SaveFilterClicked()
   }
class SearchFilterPopUpViewController: BaseViewController {
    
    @IBOutlet weak var SharingMealsSwitch: UISwitch!
    @IBOutlet weak var DietSwitch: UISwitch!
    @IBOutlet weak var FamiliesProdSwitch: UISwitch!
    
    @IBOutlet weak var GYMSwitch: UISwitch!
    @IBOutlet weak var TraditionalSwitch: UISwitch!
    @IBOutlet weak var SweetsSwitch: UISwitch!
    @IBOutlet weak var SavoryFoodSwitch: UISwitch!
    @IBOutlet weak var SaveFilterBTN: UIButton!
    var states :[String: Bool]!
    var delegate: SaveFilterClickedDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        SaveFilterBTN.Curvyimage()
       
            states = UserModel().GetFilterStates()
            SharingMealsSwitch.setOn(states["SharingMealsSwitch"] != nil ? states["SharingMealsSwitch"]! :true, animated: true)
            DietSwitch.setOn(states["DietSwitch"] != nil ? states["DietSwitch"]! :true, animated: true)
            FamiliesProdSwitch.setOn(states["FamiliesProdSwitch"] != nil ? states["FamiliesProdSwitch"]! :true, animated: true)
            GYMSwitch.setOn(states["GYMSwitch"] != nil ? states["GYMSwitch"]! :true, animated: true)
            TraditionalSwitch.setOn(states["TraditionalSwitch"] != nil ? states["TraditionalSwitch"]! :true, animated: true)
            SweetsSwitch.setOn(states["SweetsSwitch"] != nil ? states["SweetsSwitch"]! :true, animated: true)
            SavoryFoodSwitch.setOn(states["SavoryFoodSwitch"] != nil ? states["SavoryFoodSwitch"]! :true, animated: true)
        
    }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func SaveFilter(_ sender: Any) {
        UserModel().SetFilterStates(data: states)
        self.delegate.SaveFilterClicked()
        self.dismissAnimated()
        
    }
    @IBAction func SharingMeals(_ sender: Any) {
        SharingMealsSwitch.setOn(!SharingMealsSwitch.isOn, animated: true)
        states.updateValue(SharingMealsSwitch.isOn, forKey: "SharingMealsSwitch")
    }
    @IBAction func FamiliesProd(_ sender: Any) {
        FamiliesProdSwitch.setOn(!FamiliesProdSwitch.isOn, animated: true)
        states.updateValue(FamiliesProdSwitch.isOn, forKey: "FamiliesProdSwitch")
    }
    @IBAction func SavoryFood(_ sender: Any) {
        SavoryFoodSwitch.setOn(!SavoryFoodSwitch.isOn, animated: true)
        states.updateValue(SavoryFoodSwitch.isOn, forKey: "SavoryFoodSwitch")
    }
    @IBAction func Sweets(_ sender: Any) {
        SweetsSwitch.setOn(!SweetsSwitch.isOn, animated: true)
        states.updateValue(SweetsSwitch.isOn, forKey: "SweetsSwitch")
    }
    @IBAction func Traditional(_ sender: Any) {
        TraditionalSwitch.setOn(!TraditionalSwitch.isOn, animated: true)
        states.updateValue(TraditionalSwitch.isOn, forKey: "TraditionalSwitch")
    }
    @IBAction func GYM(_ sender: Any) {
        GYMSwitch.setOn(!GYMSwitch.isOn, animated: true)
        states.updateValue(GYMSwitch.isOn, forKey: "GYMSwitch")
    }
    @IBAction func Diet(_ sender: Any) {
        DietSwitch.setOn(!DietSwitch.isOn, animated: true)
        states.updateValue(DietSwitch.isOn, forKey: "DietSwitch")
    }

    }
