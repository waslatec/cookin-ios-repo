//
//  DynamicListBaseViewController.swift
//  Phunation
//
//  Created by AMIT Developer on 10/31/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class DynamicTableBaseViewController: DynamicContentBaseViewController, UITableViewDelegate, UITableViewDataSource , DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    @IBOutlet weak var tableview: UITableView!
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? data!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
    }
    
    // ===============================================
    // ==== DZNEmptyDataSet Delegate & Datasource ====
    // ===============================================
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "chooc")
    }
    
    func imageAnimation(forEmptyDataSet scrollView: UIScrollView!) -> CAAnimation! {
        let animation = CABasicAnimation(keyPath: "transform")
        
        animation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(caTransform3D: CATransform3DMakeRotation(CGFloat(Double.pi/2), 0.0, 0.0, 1.0))
        
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "No data yet !"
        
        let attributes = Font.getFontAttributesColored(Font.FONT_NAME_LIGHT, fontSize: Font.FONT_SIZE_LARG,color: UIColor.black)
        return NSAttributedString(string: text, attributes: attributes)
    }
}
