//
//  ImageDownloader.swift
//  Phunation
//
//  Created by AMIT Software on 11/6/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
class ImageDownloader {
    
    class func getImageURl(_ imgurl: String) -> URL? {
    return URL(string: Constants.ImageBaseURL.ImageURL+imgurl)!
    }
     class func downloadSingleImage(_ imageNeedsDownloadUrl: String ,callback: @escaping (UIImage?) -> ())  {
        let imagesURL = Constants.ImageBaseURL.ImageURL
        guard !imageNeedsDownloadUrl.isEmpty else {
            callback(nil)
            return
        }
        
        SDWebImageManager.shared().downloadImage(with: URL(string: "\(imagesURL)\(imageNeedsDownloadUrl)"),
                                                               options: .allowInvalidSSLCertificates,
                                                               progress: nil,
                                                               completed: {(image, error, cached, finished, url) in
                                                               
                                                                callback(image)
        })    }
    
    
    class func downloadMultipleImages(_ imagesNeedDownloadUrl: [String] ,callback: @escaping ([UIImage]?) -> ())  {
        let imagesURL = Constants.ImageBaseURL.ImageURL
        guard !imagesNeedDownloadUrl.isEmpty else {
            callback([UIImage]())
            return
        }
        let xx = UIImageView()
        var images = [UIImage]()
        for element in imagesNeedDownloadUrl
        {
        
            
            xx.sd_setImage(with: URL(string: "\(imagesURL)\(element)"), completed: { (image, err, cashe, url) in
                if image != nil
                {
                    
                    images.append(image!)
                    callback(images)
                }
            })
        }
        
//            SDWebImageManager.sharedManager().downloadImageWithURL(NSURL(string: "\(imagesURL)\(element)"),
//                                                                   options: .ProgressiveDownload,
//                                                                   progress: nil,
//                                                                   completed: {(image, error, cached, finished, url) in
//                                                                    if image != nil
//                                                                    {
//                                                                        
//                                                                        images.append(image)
//                                                                        SDWebImageManager.sharedManager().saveImageToCache(image,forURL: NSURL(string: "\(imagesURL)\(element)"))
//                                                                    }
//                                                                    
//            })}

        
//        for (index, element) in imagesNeedDownloadUrl.enumerate() {
//            SDWebImageManager.sharedManager().downloadImageWithURL(NSURL(string: "\(imagesURL)\(element)"),
//                                                                   options: .AllowInvalidSSLCertificates,
//                                                                   progress: nil,
//                                                                   completed: {[weak self] (image, error, cached, finished, url) in
//                                                                    if self != nil {
//                                                                        if image != nil
//                                                                        {
//                                                                            images.append(image)
//                                                                            if index == self!.image.count - 1 {
//                                                                                callback(images)
//                                                                            }
//                                                                        }
//                                                                    }
//                })
//        }
    
    
    }
    
}
