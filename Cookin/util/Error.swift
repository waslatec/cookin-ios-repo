//
//  Error.swift
//  Phunation
//
//  Created by AMIT Developer on 10/24/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation



enum Error: Int {
    case invalidEmail
    case invalidName
    case invalidNumber
    case noInternetConnection
    case requiredFieldsMissing
    
    // API errors
    case invalidInput = 2100
    case usernameExists = 2101
    case emailExists = 2102
    case invalidUsername = 2112
    case enterUsername = 2109
    case emptyCart = 2110
    case enterEightCharPassword = 2111
    case invalidItemsInCart = 2114

    case invalidPassword = 2104
    case userNotActivated = 2105
    
    case emailDoesNotExist = 2106 // Forgot password
    case usernameDoesNotExist = 2107 // Login
    case invalidOldPassword = 2108
    
    case unknown = 2200
}

//extension Error {
//    
//    init(apiError: APIResponses.Error) {
//        guard let code = apiError.code else {
//            self = Unknown
//            return
//        }
//        
//        guard let error = Error(rawValue: code) else {
//            self = Unknown
//            return
//        }
//        
//        self = error
//    }
//}

extension Error {
    var message: String {
        switch self {
        case .invalidName:
            return NSLocalizedString("Invalid name", comment: "")
        case .invalidItemsInCart:
            return NSLocalizedString("Invalid name", comment: "")
            
        case .enterUsername:
            return NSLocalizedString("Please enter your username/email", comment: "")
            
        case .emptyCart:
            return NSLocalizedString("No services in your wishlist", comment: "")
            
        case .invalidInput:
            return NSLocalizedString("Invalid input", comment: "")
            
        case .invalidNumber:
            return NSLocalizedString("Invalid number", comment: "")
            
        case .noInternetConnection:
            return NSLocalizedString("No internet connection", comment: "")
            
        case .requiredFieldsMissing:
            return NSLocalizedString("Please fill all highlighted entries", comment: "")
            
        case .usernameExists:
            return NSLocalizedString("This username already used", comment: "")
            
        case .usernameDoesNotExist:
            return NSLocalizedString("Username does not exist", comment: "")
            
        case .emailExists:
            return NSLocalizedString("This email already used", comment: "")
            
        case .emailDoesNotExist:
            return NSLocalizedString("This email does not exist", comment: "")
            
        case .invalidUsername:
            return NSLocalizedString("This username or email does not exist", comment: "")
            
        case .invalidPassword:
            return NSLocalizedString("Wrong password", comment: "")
            
        case .enterEightCharPassword:
            return NSLocalizedString("Password must be at least 8 characters", comment: "")
            
        case .invalidEmail:
            return NSLocalizedString("Please enter a valid email address\nexample@gmail.com", comment: "")
            
        case .userNotActivated:
            return NSLocalizedString("User is not active yet, please go to your email to activate your account", comment: "")
            
        case .invalidOldPassword:
            return NSLocalizedString("Password entered not matched with current password", comment: "")
            
        case .unknown:
            return NSLocalizedString("Something went wrong", comment: "")
        }
    }
}
