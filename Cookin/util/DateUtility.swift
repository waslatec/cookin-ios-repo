//
//  DateUtility.swift
//  Phunation
//
//  Created by yo7ia on 11/19/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

class DateUtility {

    class func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum SearchDirection {
        case next
        case previous
        
        var calendarOptions: NSCalendar.Options {
            switch self {
            case .next:
                return .matchNextTime
            case .previous:
                return [.searchBackwards, .matchNextTime]
            }
        }
    }
    
    class func get(_ direction: SearchDirection, _ dayName: Int , tody: Date, considerToday consider: Bool = false) -> Date {
        let nextWeekDayIndex = dayName // weekday is in form 1 ... 7 where as index is 0 ... 6
        
        let today = tody
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        
        if consider && (calendar as NSCalendar).component(.weekday, from: today) == nextWeekDayIndex {
            return today
        }
        
        var nextDateComponent = DateComponents()
        nextDateComponent.weekday = nextWeekDayIndex
        
        
        let date = (calendar as NSCalendar).nextDate(after: today, matching: nextDateComponent, options: direction.calendarOptions)
        return date!
    }
}
