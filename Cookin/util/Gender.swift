//
//  Gender.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

enum Gender: String {
    case Male = "m"
    case Female = "f"
    case PreferNotToSay = "o"
    case m = "Male"
    case f = "Female"
    case o = "Prefer not to say"
}

extension Gender {
    
    var displayValue: String {
        
        switch self {
        case .Male:
            return "Male"
            
        case .Female:
            return "Female"
            
        case .PreferNotToSay:
            return "Prefer not to say"
            
        case .m:
            return "m"
            
        case .f:
            return "f"
            
        case .o:
            return "o"
        }
        
    }
    
    
}