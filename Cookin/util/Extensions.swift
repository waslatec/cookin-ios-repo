//
//  Extensions.swift
//  Phunation
//
//  Created by AMIT Developer on 10/19/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import UIKit
import CCMPopup
import MapKit
extension UITabBarController {
    
    func setTabBarVisible(_ visible:Bool, animated:Bool) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // animate the tabBar
        UIView.animate(withDuration: animated ? 0.0 : 0.0, animations: {
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: visible ? Constants.ScreenHeight : Constants.ScreenHeight-49)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }) 
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < Constants.ScreenHeight
    }
}

extension NSNumber {
    
    /// Converts timestamp in seconds to NSDate
    var asDate: Date {
        return Date(timeIntervalSince1970: self.doubleValue)
    }
    
    var asString: String
        {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.string(from: self.asDate)
        }
    var asStringWithTime: String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.short
        return dateFormatter.string(from: self.asDate)
    }
}
extension String {
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    var parseJSONString: AnyObject? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if let jsonData = data {
            do {
                let x = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                return x as AnyObject?
            } catch {
                print("error serializing JSON: \(error)")
            }
            // Will return an object or nil if JSON decoding fails
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
        return nil
    }
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
}
extension UIStoryboardSegue
{
    func ccmPopUp(width: Int? = Int(Constants.ScreenWidth-50),height: Int)  {
        let popupSegue = self as! CCMPopupSegue
        
        popupSegue.destinationBounds = CGRect(x: 0, y: 0 , width: width!, height: height)
        popupSegue.backgroundBlurRadius = 0.7
        popupSegue.backgroundViewAlpha = 0.7
        popupSegue.backgroundViewColor = UIColor.black
        popupSegue.dismissableByTouchingBackground = true

    }
    }
 extension DispatchQueue {
    private static var _onceTracker = [String]()
    
    public class func once(file: String = #file, function: String = #function, line: Int = #line, block:(Void)->Void) {
        let token = file + ":" + function + ":" + String(line)
        once(token: token, block: block)
    }
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:(Void)->Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}
extension Date {
    var asString: String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.string(from: self)
    }
    var trimTime: Date {
        
        
        
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
       

//        let components = cal.components([.Day, .Month, .Year], fromDate: self)
        return (cal as NSCalendar).date(bySettingHour: 2, minute: 0, second: 0, of: self, options: NSCalendar.Options())!
    }
    
}

extension UIImage {
    
    /**
     Tint, Colorize image with given tint color<br><br>
     This is similar to Photoshop's "Color" layer blend mode<br><br>
     This is perfect for non-greyscale source images, and images that have both highlights and shadows that should be preserved<br><br>
     white will stay white and black will stay black as the lightness of the image is preserved<br><br>
     
     <img src="http://yannickstephan.com/easyhelper/tint1.png" height="70" width="120"/>
     
     **To**
     
     <img src="http://yannickstephan.com/easyhelper/tint2.png" height="70" width="120"/>
     
     - parameter tintColor: UIColor
     
     - returns: UIImage
     */
        enum JPEGQuality: CGFloat {
            case lowest  = 0
            case low     = 0.25
            case medium  = 0.5
            case high    = 0.75
            case highest = 1
        }
        
        /// Returns the data for the specified image in PNG format
        /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
        /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
        var png: Data? { return UIImagePNGRepresentation(self) }
        
        /// Returns the data for the specified image in JPEG format.
        /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
        /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
        func jpeg(_ quality: JPEGQuality) -> Data? {
            return UIImageJPEGRepresentation(self, quality.rawValue)
        }
    
    func tintPhoto(_ tintColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw black background - workaround to preserve color of partially transparent pixels
            context.setBlendMode(.normal)
            UIColor.black.setFill()
            context.fill(rect)
            
            // draw original image
            context.setBlendMode(.normal)
            context.draw(cgImage!, in: rect)
            
            // tint image (loosing alpha) - the luminosity of the original image is preserved
            context.setBlendMode(.color)
            tintColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(context.makeImage()!, in: rect)
        }
    }
    
    /**
     Tint Picto to color
     
     - parameter fillColor: UIColor
     
     - returns: UIImage
     */
    func tintPicto(_ fillColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw tint color
            context.setBlendMode(.normal)
            fillColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(cgImage!, in: rect)
        }
    }
    
    /**
     Modified Image Context, apply modification on image
     
     - parameter draw: (CGContext, CGRect) -> ())
     
     - returns: UIImage
     */
    fileprivate func modifiedImage(_ draw: (CGContext, CGRect) -> ()) -> UIImage {
        
        // using scale correctly preserves retina images
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context: CGContext! = UIGraphicsGetCurrentContext()
        assert(context != nil)
        
        // correctly rotate image
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        
        draw(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
extension UINavigationController
{
    func shareImageAndText(image: UIImage,text: String)
    {
        var messageStr :NSAttributedString?
        
            
            let str =  NSAttributedString(string: text)
            messageStr  = str
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems:  [image, messageStr!], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
            self.present(activityViewController, animated: true, completion: nil)
            
       

    }
}
extension UIView
{
    func roundedimage()
    {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    func Curvyimage()
    {
        self.layer.cornerRadius = self.frame.size.width / 15
        self.clipsToBounds = true
    }
    func AddBorderWithColor(radius:Int? = 15,borderWidth:Int? = 0,color:String? = "01B0F0" )
    {
        self.layer.cornerRadius = CGFloat(radius!)
        self.layer.borderWidth = CGFloat(borderWidth!)
        self.layer.borderColor = UIColor(fromARGBHexString: color).cgColor
        self.clipsToBounds = true

    }
}
extension CLLocation
{
    func DistanceBetweenTwoLocations(lat: Double,lng: Double) -> String {
        let loc = CLLocation(latitude: lat, longitude: lng)
       let distance =  self.distance(from: loc)
        if distance > 1000
        {
            return "\((distance/1000).rounded()) KM ".replacingOccurrences(of: ".0", with: "")
            
        }
        else
        {
            return "\(distance.rounded()) M "
        }
    }
}
extension UITextView
{
    func addPadding() {
        self.contentInset = UIEdgeInsetsMake(5, 5, 5, 5);
        
    }
}
extension UITextField
{
    
     func addPadding() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.rightView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
        self.rightViewMode = UITextFieldViewMode.always

    }
    func showExclamationMark() {
        self.rightViewMode = .always
        let exclamationIV = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        exclamationIV.image = UIImage(named: "exclamation")
        exclamationIV.contentMode = .scaleAspectFit
        self.rightView = exclamationIV
    }
    
    func hideExclamationMark() {
        self.rightViewMode = .never
        self.rightView = nil
    }

    func setBottomBorder(_ color:UIColor)
    {
        self.borderStyle = UITextBorderStyle.none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height+15 - width,   width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
    }
}

extension String {
    
    func heightForWithFont( _ width: CGFloat) -> CGFloat {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width + 5 + 5, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = self
        label.sizeToFit()
        return label.frame.height + 10
    }
    
    var trimmed: String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    var toDate: Date {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.date(from: self)!
        
    }
    
    
}

extension Double {
    var round : String
    {
    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = 0
    formatter.maximumFractionDigits = 1
        return  formatter.string(from: NSNumber(value: self))!

    }
}
extension UISegmentedControl {
    func removeBorders() {
        setBackgroundImage(imageWithColor(UIColor.clear), for: UIControlState(), barMetrics: .default)
        setBackgroundImage(imageWithColor(UIColor.clear), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(UIColor.clear), forLeftSegmentState: UIControlState(), rightSegmentState: UIControlState(), barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    fileprivate func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 10.0, height: 20.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor);
        context?.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
    
}






