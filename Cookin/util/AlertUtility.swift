//
//  AlertUtility.swift
//  Le Cadeau
//
//  Created by Mohammad Shaker on 1/21/16.
//  Copyright © 2016 AMIT-Software. All rights reserved.
//

import Foundation
import SCLAlertView
import UserNotifications
class AlertUtility {
    
    class func showErrorAlert(_ message: String!) {
        SCLAlertView().showTitle(
            NSLocalizedString("Error", comment: "Error"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .error,
            colorStyle: 0x379AE1,
            colorTextButton: 0xFFFFFF
        )
    }
    
    class func scheduleNotification(at date: Date,title: String,body: String) {
//        let calendar = Calendar(identifier: .gregorian)
//        let components = calendar.dateComponents(in: .current, from: date)
//        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
//        
//        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        if #available(iOS 10.0, *) {
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 2, repeats: false)
        

        
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        content.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
        
        let request = UNNotificationRequest(identifier: "\(date)", content: content, trigger: trigger)
        
//        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
        } else {
            // Fallback on earlier versions
        }
    }
    class func showSuccessAlert(_ message: String!) {
        SCLAlertView().showTitle(
            NSLocalizedString("Note", comment: "Note"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .success,
            colorStyle: 0x379AE1,
            colorTextButton: 0xFFFFFF
        )
    }
    
    class func showAlertWithButton(msg: String ,title: String , buttonTitle: String, callback: @escaping () -> Void )
    {
        let alertView = SCLAlertView()
        alertView.addButton(buttonTitle) {
            callback()
//            AlertUtility.showErrorAlert("sss")
        }
        alertView.showTitle(
            title,
            subTitle: msg,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .notice,
            colorStyle: 0x379AE1,
            colorTextButton: 0xFFFFFF
        )
        
    }
    class func showErrorAlertWithSignUpOption(_ message: String ,parameter: String) {
        let alertView = SCLAlertView()
        alertView.addButton(NSLocalizedString("Resend Activation", comment: "")) {
           /* UserModel.ResendActivation(parameter) { (user, err, errCode) in
                
                if !self.handleNetworkError(user ,msg: err, errorCode: 0) {
                    //LOGIC
                    showSuccessAlert("Activation email had been sent")
                }
            }*/
            }
        alertView.showTitle(
            NSLocalizedString("Note", comment: "Note"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .error,
            colorStyle: 0x379AE1,
            colorTextButton: 0xFFFFFF
        )
    }
    class func handleNetworkError(_ response: AnyObject? , msg: String?, errorCode: Int?) -> Bool{
        if (msg == nil &&  errorCode == BaseModel.SUCCESS_ERROR_CODE) {
            return false
        }
        let data = response as! NSDictionary
        let erro = data["code"] as! NSString
        AlertUtility.showErrorAlert(Error(rawValue: (Int(erro as String))!)?.message)
        print("\((response as! NSDictionary)["code"]!) - \(msg!) - \(erro)")
        
        //Do error handling
        return true
    }
}
