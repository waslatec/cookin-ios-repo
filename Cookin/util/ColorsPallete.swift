//
//  ColorsPallete.swift
//  Phunation
//
//  Created by AMIT Developer on 10/31/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit

class ColorsPallete {
    static let COLOR_LGHT_BLUE = UIColor(red: 122.0/255.0, green: 199.0/255.0, blue: 255.0/255.0, alpha: 1)
    static let COLOR_DARK_BLUE = UIColor(red: 38.0/255.0, green: 143.0/255.0, blue: 217.0/255.0, alpha: 1)
}