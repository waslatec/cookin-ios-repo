//
//  Font.swift
//  Phunation
//
//  Created by AMIT Developer on 10/31/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit

class Font  {
    
    //TODO: make an enum of titles and sizes
    static let FONT_NAME_LIGHT = "HelveticaNeue-UltraLight"
    static let FONT_NAME_THIN = "HelveticaNeue-UltraLight"
    static let FONT_NAME_BOLD = "HelveticaNeue-UltraLight"
    
    static let FONT_SIZE_SMALL: CGFloat = 10
    static let FONT_SIZE_MED: CGFloat = 20
    static let FONT_SIZE_LARG: CGFloat = 26
    
    class func getFontAttributes(_ fontName: String, fontSize:CGFloat) -> [String : AnyObject] {
        return [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName : UIFont (name :fontName, size: fontSize)!]
    }
    class func getFontAttributesColored(_ fontName: String, fontSize:CGFloat , color: UIColor) -> [String : AnyObject] {
        return [NSForegroundColorAttributeName : color, NSFontAttributeName : UIFont (name :fontName, size: fontSize)!]
    }
}
