//
//  SearchMapViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
import MapKit

class SearchMapViewController: UIViewController,CLLocationManagerDelegate,SaveFilterClickedDelegate {
    @IBOutlet weak var mapview: MKMapView?
    var locationManager = CLLocationManager()
    public var currentuserLocation: CLLocation!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        handleLocationUpdated(locations: locations)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filterpopUp"
        {
            segue.ccmPopUp(height: 438)
            let vc = segue.destination as! SearchFilterPopUpViewController
            vc.delegate = self
            vc.view.AddBorderWithColor(radius: 15, borderWidth: 4, color: "01B0F0")
            
        }
    }
    
    func SaveFilterClicked() {
        
    }
    func handleLocationUpdated(locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        mapview?.mapType = MKMapType.standard
        if currentuserLocation == nil
        {
            currentuserLocation = userLocation
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: userLocation.coordinate, span: span)
            mapview?.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = userLocation.coordinate
            annotation.title = "My location"
            annotation.subtitle = ""
            mapview?.addAnnotation(annotation)
            CurrentUserLocation = currentuserLocation
            getData()

        }
        else
        {
            if currentuserLocation.distance(from: userLocation) > 500
            {
//                let oldannotation = MKPointAnnotation()
//                oldannotation.coordinate = currentuserLocation.coordinate
//                oldannotation.title = "My location"
//                oldannotation.subtitle = ""
//                mapview?.removeAnnotation(oldannotation)
                currentuserLocation = userLocation
                let span = MKCoordinateSpanMake(0.05, 0.05)
                let region = MKCoordinateRegion(center: userLocation.coordinate, span: span)
                mapview?.setRegion(region, animated: true)
                

                CurrentUserLocation = currentuserLocation
                getData()

            }
            
        }

    }
    func getData()
    {
        if CurrentUserLocation != nil
        {
        UserModel.SearchMeals(data: ["MapLat": CurrentUserLocation!.coordinate.latitude as AnyObject,"MapLng": CurrentUserLocation!.coordinate.latitude as AnyObject], callback: { (dataa, error, errCode) in
            let x = dataa as! [[String: AnyObject]]
            self.mapview?.removeAnnotations((self.mapview?.annotations)!)
            for i in x
            {
                let m = Meal(JSON: i)!
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: m.MapLat, longitude: m.MapLng)
                annotation.title = m.ClientName
                annotation.subtitle = m.Name
                
                
                self.mapview?.addAnnotation(annotation)
                
            }
            let annotation = MKPointAnnotation()
            annotation.coordinate = (CurrentUserLocation?.coordinate)!
            annotation.title = "My location"
            annotation.subtitle = ""
            self.mapview?.addAnnotation(annotation)
        })
        }
    }

}
