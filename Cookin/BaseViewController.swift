//
//  BaseViewController.swift
//  Phunation
//
//  Created by AMIT Developer on 10/23/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

//TODO:
/*
 extract all functions to Util class
 all functions should take parameter of type UIViewController
 Call all these functions here in base
 */

class BaseViewController: UIViewController , UITextFieldDelegate{
    //TODO: make an enum
    var navigationBarStyle: String = "default"
    let tabbar :UITabBarController = UITabBarController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupView()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNavigationBar(self.navigationBarStyle)
    }
    
    func SetupView()  {
        
    }
    
    func setUpNavigationBar(_ style: String) {
        if style ==  "default" {
            self.tabBarController?.navigationItem.hidesBackButton = true
            navigationController?.navigationBar.isHidden = false
            
            navigationController?.navigationBar.tintColor = UIColor.black
        } else {
            self.tabBarController?.navigationItem.hidesBackButton = true
            navigationController?.navigationBar.isHidden = false
            navigationController?.navigationBar.tintColor = UIColor.white
        }
        
    }
    
    func handleNetworkError(_ response: AnyObject? , msg: String?, errorCode: Int? ,username: String?) -> Bool {
        if (msg == nil &&  errorCode == BaseModel.SUCCESS_ERROR_CODE) {
            return false
        }
        let data = response as! NSDictionary
//        let erro = data["code"] as! NSString
//        if erro == "2105"
//        {
//            AlertUtility.showErrorAlertWithSignUpOption((Error(rawValue: (Int(erro as String))!)?.message)!, parameter: username!)
//        }
//        
//        else
//        {
//            AlertUtility.showErrorAlert(Error(rawValue: (Int(erro as String))!)?.message)
//        }
//        print("\((response as! NSDictionary)["code"]!) - \(msg) - \(erro)")
//        
        //Do error handling
        AlertUtility.showErrorAlert(data["EnglishMessage"] as! String)
        return true
    }
    
    //TODO:
    //Bool -> enum (singin, up , forget
    // Add other parameter (confirmFirst: Bool)

    
    func showSignInDialog(_ signUp: Bool) {
        let loginVC = signUp ? "SignUpNavigationController" :  "SignInNavigationController"
        let nc = self.storyboard!.instantiateViewController(withIdentifier: loginVC) as! UINavigationController
//        nc.performSegue(withIdentifier: "gotologin", sender: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nc
//        self.dismissToLogin()
    }
    
    func dismissToLogin() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    
}
