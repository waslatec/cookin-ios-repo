//
//  ChangePasswordVC.swift
//  Le Cadeau
//
//  Created by Mohammad Shaker on 1/20/16.
//  Copyright © 2016 AMIT-Software. All rights reserved.
//

import UIKit


class ChangePasswordVC: BaseViewController {

    
    @IBOutlet weak var currentPasswordTF: UITextField!
    var successfullCompletionHandler: ((Void) -> Void)?
    
    var userMgr = UserModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    func toggleTextFieldSecureState(_ textField: UITextField) {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        
        if textField.isFirstResponder {
            textField.becomeFirstResponder()
        }
    }
    
    
    @IBAction func toggleCurrentPassword() {
        toggleTextFieldSecureState(currentPasswordTF)
    }
    
    
   
    
    
    @IBAction func dismiss() {
        self.dismissAnimated()
    }
    
    
       
    
    
    fileprivate func validateAll() -> Bool
        // Validate password
    {
        if (!Validation.isValidPassword(currentPasswordTF.text!)) {
            AlertUtility.showErrorAlert("Password must be at least 8 characters")
            return false
        }
        
        
        return true
    }
    
    
    @IBAction func changePassword() {
        self.view.endEditing(true)
        
       
        
        if (!self.validateAll()) {
            return
        }
        let params:[String: AnyObject] = ["NewPassword": currentPasswordTF.text! as AnyObject]
        UserModel.ChangePassword(data: params) { (user, err, errCode) in
            
            if !super.handleNetworkError(user ,msg: err, errorCode: 0,username: "") {
                //LOGIC
//                ChangePasswordVC.dismissAnimated()
                UserModel.setUserPassword(self.currentPasswordTF.text!)
                self.successfullCompletionHandler?()

            }
        }

    }
}


// =====================================
// ==== UITextFieldDelegate Methods ====
// =====================================

//extension ChangePasswordVC: UITextFieldDelegate {
//    
//    override func textFieldShouldReturn(textField: UITextField) -> Bool {
//        switch textField {
//        case currentPasswordTF:
//            newPasswordTF.becomeFirstResponder()
//        case newPasswordTF:
//            changePassword()
//        default: break
//        }
//        
//        return true
//    }
//    
//}
