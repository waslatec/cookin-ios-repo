//
//  SettingsTViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/5/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import Foundation
import UIKit
class SettingsTViewController: UITableViewController{
    
    @IBOutlet fileprivate weak var Name: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.Name.text! = LoggedUserProfile!.UserName
    }
    
   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 1:
            performSegue(withIdentifier: "Account", sender: nil)
            break
        case 2:
            performSegue(withIdentifier: "Account", sender: nil)
            break
        case 3:
            performSegue(withIdentifier: "Account", sender: nil)
            break
        case 4:
            performSegue(withIdentifier: "About", sender: nil)
            break
        case 5:
            performSegue(withIdentifier: "Account", sender: nil)
            break
        case 6:
            performSegue(withIdentifier: "Notification", sender: nil)
            break
        case 9:
            UserModel.logout()
            let nc = self.storyboard!.instantiateViewController(withIdentifier: "OnBoarding")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nc
            break
        default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "Notification":
                segue.ccmPopUp(height: 280)
                segue.destination.view.AddBorderWithColor(radius: 15, borderWidth: 4, color: "01B0F0")
            break
        default:
            break
        }
    }
    
}
