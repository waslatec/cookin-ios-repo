//
//  NetworkMangar.swift
//  Phunation
//
//  Created by AMIT Developer on 10/24/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

protocol NetworkManager {
    
    func doGet(_ url: String, params:[String: String]?, callback: @escaping (AnyObject, String?, Int) -> Void);
    func doPost(_ url: String, params:[String: AnyObject]!,CheckProgress: Bool?, callback: @escaping (AnyObject, String?, Int) -> Void);
    func doPostMultiPart(NickName: String,UserName: String, Password: String,PhoneNumber : String ,Email: String ,BirthDate: String ,MapLat: Double ,MapLng: Double ,CityId: Int ,isCheef: Bool, LogoPath: Data? , BannerPath : Data?, url: String, callback:  @escaping (AnyObject, String?, Int) -> Void);
    func doPostAddMeal(MealName: String,MealDescription: String, MealPrice: String,MealUnitNumber : String ,MealTypeID: Int ,ClientId: String ,Weight: Double ,isHeat: Bool, Image1: Data? , Image2 : Data?, Image3 : Data?, Image4 : Data?, url: String, callback:  @escaping (AnyObject, String?, Int) -> Void);
    
    
}
