//
//  AlamoNetworkManager.swift
//  Phunation
//
//  Created by yo7ia on 2/16/17.
//  Copyright © 2017 AMIT Software. All rights reserved.
//



import Foundation
import Alamofire
import Reachability

let reachabilityy = Reachability.forInternetConnection()

class AlamoNetworkManager: NSObject, NetworkManager {
    
    
    
    
    
    func doGet(_ url: String, params:[String: String]?, callback:  @escaping (AnyObject, String?, Int) -> Void){
        
    }
    
    func doPostAddMeal(MealName: String,MealDescription: String, MealPrice: String,MealUnitNumber : String ,MealTypeID: Int ,ClientId: String ,Weight: Double ,isHeat: Bool, Image1: Data? , Image2 : Data?, Image3 : Data?, Image4 : Data?, url: String, callback:  @escaping (AnyObject, String?, Int) -> Void)  {
        guard (reachabilityy?.isReachable())! else {
            AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
            return
        }
        
        ProgressUtility.showProgressViewWithProgress(0)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if Image1 != nil{
                multipartFormData.append(Image1!, withName: "MealPhoto1",fileName: "\(Date().trimTime.timeIntervalSince1970)MealPhoto1.jpeg", mimeType: "image/jpeg")
            }
            if Image2 != nil{
                multipartFormData.append(Image2!, withName: "MealPhoto2",fileName: "\(Date().trimTime.timeIntervalSince1970)MealPhoto2.jpeg", mimeType: "image/jpeg")
            }
            if Image3 != nil{
                multipartFormData.append(Image3!, withName: "MealPhoto3",fileName: "\(Date().trimTime.timeIntervalSince1970)MealPhoto3.jpeg", mimeType: "image/jpeg")
            }
            if Image4 != nil{
                multipartFormData.append(Image4!, withName: "MealPhoto4",fileName: "\(Date().trimTime.timeIntervalSince1970)MealPhoto4.jpeg", mimeType: "image/jpeg")
            }
            
            multipartFormData.append(MealDescription.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "Description")
            
            multipartFormData.append(String(isHeat).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "isHeat")
            
            multipartFormData.append(String(Weight).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "Weight")
            
            multipartFormData.append(MealUnitNumber.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "UnitNumber")
            
            multipartFormData.append("true".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "isAviliable")
            
            multipartFormData.append("10".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "MealTime")
            
            multipartFormData.append(MealPrice.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "Price")
            
            multipartFormData.append(MealName.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "MealName")
            
            multipartFormData.append(String(MealTypeID).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "MealTypeId")
            
            multipartFormData.append(ClientId.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "ClientId")
            
        }, usingThreshold: 1, to: url, method: .post, headers: ["content-type": "multipart/form-data",
                                                                "authorization": LoggedUser!.BearerToken], encodingCompletion: { encodingResult in
                                                                    
                                                                    switch encodingResult {
                                                                    case .success(let upload, _, _):
                                                                        upload.uploadProgress(closure: { (shi) in
                                                                            ProgressUtility.showProgressViewWithProgress(Float(shi.fractionCompleted))
                                                                        })
                                                                        upload.response(completionHandler: { (response) in
                                                                            print(response)
                                                                            ProgressUtility.dismissProgressView()
                                                                            
                                                                            
                                                                        })
                                                                        upload.responseString(completionHandler: { (data) in
                                                                            print(data)
                                                                        })
                                                                        upload.responseJSON { response in
                                                                            print(response)
                                                                            ProgressUtility.dismissProgressView()
                                                                            let data = response.result.value as! [String: AnyObject]?
                                                                            
                                                                            if data != nil
                                                                            {
                                                                            if let _ = data!["Success"]
                                                                            {
                                                                                if(data!["Success"] as! Bool)
                                                                                {
                                                                                    callback(data as AnyObject, nil, BaseModel.SUCCESS_ERROR_CODE)
                                                                                    
                                                                                }
                                                                                else
                                                                                {
                                                                                
                                                                                    callback(data as AnyObject, data!["EnglishMessage"] as! String?, BaseModel.SUCCESS_ERROR_CODE)
                                                                                    
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                AlertUtility.showErrorAlert(data!["Message"] as! String)
                                                                            }
                                                                            }
                                                                            else
                                                                            {
                                                                                 AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                    case .failure(let encodingError):
                                                                        print(encodingError)
                                                                        ProgressUtility.dismissProgressView()
                                                                        
                                                                        AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                                                                    }
        })
        
    }
    
    
    func doPostMultiPart( NickName: String,UserName: String, Password: String,PhoneNumber : String ,Email: String ,BirthDate: String ,MapLat: Double ,MapLng: Double ,CityId: Int ,isCheef: Bool, LogoPath: Data? , BannerPath : Data?, url: String, callback:  @escaping (AnyObject, String?, Int) -> Void)  {
        guard (reachabilityy?.isReachable())! else {
            AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
            return
        }
        
        ProgressUtility.showProgressViewWithProgress(0)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if LogoPath != nil{
                multipartFormData.append(LogoPath!, withName: "ProfilePhotoPath",fileName: "\(Date().trimTime.timeIntervalSince1970)ProfilePhotoPath.jpeg", mimeType: "image/jpeg")
            }
            if BannerPath != nil{
                multipartFormData.append(BannerPath!, withName: "CoverPhotoPath",fileName: "\(Date().trimTime.timeIntervalSince1970)CoverPhotoPath.jpeg", mimeType: "image/jpeg")
            }
            multipartFormData.append(NickName.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "NickName")
            multipartFormData.append(UserName.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "UserName")
            multipartFormData.append(Password.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "Password")
            multipartFormData.append(PhoneNumber.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "PhoneNumber")
            multipartFormData.append(Email.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "Email")
            multipartFormData.append(BirthDate.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "BirthDate")
            multipartFormData.append(String(MapLat).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "MapLat")
            multipartFormData.append(String(MapLng).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "MapLng")
            multipartFormData.append(String(isCheef).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "isCheef")
            multipartFormData.append(String(CityId).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "CityId")
        }, usingThreshold: 1, to: url, method: .post, headers: ["content-type": "multipart/form-data",
                                                                "authorization": LoggedUser!.BearerToken], encodingCompletion: { encodingResult in
                                                                    
                                                                    switch encodingResult {
                                                                    case .success(let upload, _, _):
                                                                        upload.uploadProgress(closure: { (shi) in
                                                                            ProgressUtility.showProgressViewWithProgress(Float(shi.fractionCompleted))
                                                                        })
                                                                        upload.response(completionHandler: { (response) in
                                                                            print(response)
                                                                            ProgressUtility.dismissProgressView()
                                                                            
                                                                            
                                                                        })
                                                                        upload.responseString(completionHandler: { (data) in
                                                                            print(data)
                                                                        })
                                                                        upload.responseJSON { response in
                                                                            print(response)
                                                                            ProgressUtility.dismissProgressView()
                                                                            let data = response.result.value as! [String: AnyObject]?
                                                                            //                    if response.response?.statusCode != 200
                                                                            //                    {
                                                                            //                        if let data = response.result.value as! [String: AnyObject]?{
                                                                            //                            if(data["code"] == nil)
                                                                            //                            {
                                                                            //                               AlertUtility.showErrorAlert(data["message"] as! String)
                                                                            //                            }
                                                                            //                            else
                                                                            //                            {
                                                                            //                                callback(data as AnyObject, "error", BaseModel.SUCCESS_ERROR_CODE)
                                                                            //
                                                                            //                            }
                                                                            //                            //                    print(response.result.value)
                                                                            //                        }
                                                                            //
                                                                            //                    }
                                                                            //                    else
                                                                            //                    {
                                                                            //                        if let data = response.result.value as! [String: AnyObject]?{
                                                                            //                            if(data["code"] == nil)
                                                                            //                            {
                                                                            //                                callback(response as AnyObject, nil, BaseModel.SUCCESS_ERROR_CODE)
                                                                            //                                
                                                                            //                            }
                                                                            //                            else
                                                                            //                            {
                                                                            //                                callback(data as AnyObject, "error", BaseModel.SUCCESS_ERROR_CODE)
                                                                            //                                
                                                                            //                            }
                                                                            //                            //                    print(response.result.value)
                                                                            //                        }
                                                                            //
                                                                            //                    }
                                                                            if let _ = data!["Success"]
                                                                            {
                                                                                if(data!["Success"] as! Bool)
                                                                                {
                                                                                    callback(data as AnyObject, nil, BaseModel.SUCCESS_ERROR_CODE)
                                                                                    
                                                                                }
                                                                                else
                                                                                {
//                                                                                    let erro = data!["EnglishMessage"] as! NSString
                                                                                    
                                                                                    //                        if erro == "2101" || erro == "2102" || erro == "2113" || erro == "2100"
                                                                                    //                        {
                                                                                    //                            let msg = data["message"] as! [String: AnyObject]
                                                                                    ////                            AlertUtility.showErrorAlert()
                                                                                    //                            callback(data as AnyObject, ((data["message"] as! [String: AnyObject]).first.customMirror.children.first?.value as! (String,AnyObject)).1.allObjects[0] as! String, BaseModel.SUCCESS_ERROR_CODE)
                                                                                    //
                                                                                    //                        }
                                                                                    //                        else
                                                                                    //                        {
                                                                                    
                                                                                    callback(data as AnyObject, data!["EnglishMessage"] as! String?, BaseModel.SUCCESS_ERROR_CODE)
                                                                                    
                                                                                    //                            AlertUtility.showErrorAlert(Error(rawValue: (Int(erro as String))!)?.message)
                                                                                    //                            print("\((response as! NSDictionary)["code"]!) - \(msg) - \(erro)")
                                                                                    //                        }
                                                                                    
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                AlertUtility.showErrorAlert(data!["Message"] as! String)
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                    case .failure(let encodingError):
                                                                        print(encodingError)
                                                                        ProgressUtility.dismissProgressView()
                                                                        
                                                                        AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                                                                    }
        })
        
    }
    func doPost(_ url: String, params:[String: AnyObject]!,CheckProgress: Bool? , callback:  @escaping (AnyObject, String?, Int) -> Void) {
        if CheckProgress == true
        {
            guard (reachabilityy?.isReachable())! else {
                AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                return
            }
            ProgressUtility.showProgressView()
        }
        var headers: HTTPHeaders?
        if LoggedUser != nil
        {
            headers = [
                "authorization": LoggedUser!.BearerToken
            ]
            
        }
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            if CheckProgress == true
            {
                ProgressUtility.dismissProgressView()
                
                switch(response.result) {
                case .success(_):
                    if response.result.value is NSArray
                    {
                        callback(response.result.value as AnyObject, nil, BaseModel.SUCCESS_ERROR_CODE)
                        
                    }
                    else
                    {
                        if let data = response.result.value as? [String: AnyObject]?{
                            print(data!)
                            if let _ = data!["Success"]
                            {
                                if(data!["Success"] as! Bool)
                                {
                                    callback(data as AnyObject, nil, BaseModel.SUCCESS_ERROR_CODE)
                                    
                                }
                                else
                                {
                                    _ = data!["EnglishMessage"] as! NSString
                                    
                                    //                        if erro == "2101" || erro == "2102" || erro == "2113" || erro == "2100"
                                    //                        {
                                    //                            let msg = data["message"] as! [String: AnyObject]
                                    ////                            AlertUtility.showErrorAlert()
                                    //                            callback(data as AnyObject, ((data["message"] as! [String: AnyObject]).first.customMirror.children.first?.value as! (String,AnyObject)).1.allObjects[0] as! String, BaseModel.SUCCESS_ERROR_CODE)
                                    //
                                    //                        }
                                    //                        else
                                    //                        {
                                    
                                    callback(data as AnyObject, data!["EnglishMessage"] as! String?, BaseModel.SUCCESS_ERROR_CODE)
                                    
                                    //                            AlertUtility.showErrorAlert(Error(rawValue: (Int(erro as String))!)?.message)
                                    //                            print("\((response as! NSDictionary)["code"]!) - \(msg) - \(erro)")
                                    //                        }
                                    
                                }
                            }
                            else
                            {
                                AlertUtility.showErrorAlert(data!["Message"] as! String)
                            }
                            //                    print(response.result.value)
                        }
                        else
                        {
                            AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                        }
                    }
                    break
                    
                case .failure(_):
                    AlertUtility.showErrorAlert("No internet connection\nPlease turn on Wifi/Data Cellular")
                    return
                    
                    //                break
                    
                }
            }
        }
    }
}
