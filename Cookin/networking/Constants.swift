//
//  Constants.swift
//  Phunations
//
//  Created by Yo7ia on 11/10/16.
//  Copyright © 2016 AMIT. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    fileprivate static let ScreenSize: CGRect = UIScreen.main.bounds
    static let ScreenWidth = ScreenSize.width
    static let ScreenHeight = ScreenSize.height
    static let DeviceID = UIDevice.current.identifierForVendor!.uuidString

    struct ImageBaseURL {
        static let ImageURL = "https://s3-us-west-2.amazonaws.com/phonation/uploads/"
    }
    enum SortType: Int {
        case popular = 0
        case recent = 1
    }
    enum FilterType: Int {
        case New = 0
        case Pending = 1
        case Rejected = 2
        case Approved = 3
    }
    
    enum RequestType: Int {
        case None = 0
        case Approved = 1
        case Rejected = 2
    }
    
    struct PaymentConstants {
        struct Production {
            static let AccessCode = "gpgdDeGH1tgq3VOjLUff"
            static let Language = "en"
            static let MerchantIdentifier = "ocJjDYpc"
            static let ServiceCommand = "SDK_TOKEN"
            static let TestShain = "TESTSHAIN"
        }
        struct Development {
            static let AccessCode = "gpgdDeGH1tgq3VOjLUff"
            static let Language = "en"
            static let MerchantIdentifier = "ocJjDYpc"
            static let ServiceCommand = "SDK_TOKEN"
            static let TestShain = "TESTSHAIN"
        }
    }
    
    struct UserDefaultsConstants {
        static let Email = "email"
        static let UserData = "UserData"
        static let Password = "password"
        static let FirstTime = "firstTime"
        static let Token = "token"
    }
    //    public static final String SYNC_URL = "http://52.42.186.30:4984/phonation-syncgw";
    //    public static final String SERVERURL = "http://52.42.186.30/";
    
    struct Couch {
        static let DatabaseName = "phonation-syncgw"
   //     static let DatabaseName = "phonation"
        
        static let SyncgatewayURL = URL(string: "http://52.42.186.30:4984/\(DatabaseName)")!
        
        struct DocumentType {
            static let UserProfile = "user_profile"
            static let UserCart = "user_cart"
            static let UserOrders = "order"
            static let UserNotification = "user_notification"
            
            static let Category = "category"
            
            static let ProviderCalender = "provider_calender"
            static let Provider = "provider_profile"
            
            static let ServiceReview = "review"
            static let Service = "service"
        }
        
        struct Notification {
            static let CategoryUpdated = "CategoryUpdated"
            static let ServiceUpdated = "ServiceUpdated"
            static let SearchServiceUpdated = "SearchServiceUpdated"
            static let ConfigsUpdated = "ConfigsUpdated"

            static let SingleServiceUpdated = "SingleServiceUpdated"
            static let ServicesPerCategory = "ServicesPerCategory"
            static let ProviderProfile = "ProviderProfile"
            static let ProviderCalendar = "ProviderCalendar"
            
            static let ServicesPerProvider = "ServicesPerProvider"

            static let UserProfile = "UserProfile"
            static let UserCartUpdated = "UserCartUpdated"
            static let UserOrdersUpdated = "UserOrdersUpdated"
            static let ProviderRequestUpdated = "ProviderRequestUpdated"
            static let ProviderTransactionsUpdated = "ProviderTransactionsUpdated"
            
            static let ServiceReviewUpdated = "ServiceReviewUpdated"
            
            
        }
        
        struct View {
            
            static let AllServices = "AllServices"
            static let SearchService = "SearchService"
            static let Configs = "Configs"
            static let AllCategories = "AllCategories"
            static let ServicesPerCategory = "ServicesPerCategory"
            static let UserProfile = "UserProfile"
            static let UserCart = "UserCart"
            static let UserOrders = "UserOrders"
            static let ServiceReview = "ServiceReview"
            static let ProviderProfile = "ProviderProfile"
            static let ProviderCalendar = "ProviderCalendar"
            static let ProviderRequest = "ProviderRequest"
            static let ProviderTransactions = "ProviderTransactions"
            
            static let ServicesPerProvider = "ServicesPerProvider"

            
            struct Version {
                static let AllServices = "0.0.0.2"
                static let AllCategories = "0.0.0.2"
                static let ServicesPerCategory = "0.0.0.2"
                static let UserProfile = "0.0.0.2"
                static let UserCart = "0.0.0.2"
                static let UserOrders = "0.0.0.2"

                static let ProviderProfile = "0.0.0.2"
                static let ProviderCalendar = "0.0.0.2"
                static let ProviderRequest = "0.0.0.3"
                static let ProviderTransactions = "0.0.0.3"
                
                static let ServicesPerProvider = "0.0.0.2"
                static let ServiceReview = "0.0.0.2"
                static let SearchServices = "0.0.0.7"
                
            }
        }
    }
    
    struct APIProvider {
      static let APIBaseURL = "http://api.coockin.wasltec.com/"
        
        static let Login = APIBaseURL + "api/Users/Login"
        static let Signup = APIBaseURL + "api/Users/register"
        static let ConfirmCode = APIBaseURL + "api/Users/confirm"
        static let ForgetPassword = APIBaseURL + "api/Users/ForgetPassword"
        static let GetProfile = APIBaseURL + "api/Client/GetProfile"
        static let UpdateProfile = APIBaseURL + "api/Client/UpdateProfile"
        static let SearchMeals = APIBaseURL + "api/Meal/SearchMeals"
        static let GetCheifMeals = APIBaseURL + "api/Meal/GetCheifMeals"
        static let AddMeal = APIBaseURL + "api/Meal/AddMeal"
        static let UpdatePassword = APIBaseURL + "api/Client/UpdatePassword"
        static let UpdateEmail = APIBaseURL + "api/Client/UpdateMail"

        static let ValidateProfile = APIBaseURL + "api/users/profile-validate"
        static let ResetPassword = APIBaseURL + "api/auth/forgot-password"
        static let ResendActivation = APIBaseURL + "api/auth/resend-activation"
        static let RegisterNotification = APIBaseURL + "api/auth/register-push-token"
        static let CheckAvailibilty = APIBaseURL + "api/users/check-availability"
        static let CancelRequest = APIBaseURL + "api/users/cancel-request"
        static let ApproveOrReject = APIBaseURL + "api/users/action"
        static let ChangePassword = APIBaseURL + "api/auth/change-password"
        static let Payment = APIBaseURL + "api/users/payment-version3"
        static let RequestTokenForPayment = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi"
        
        
    }
    
}
