//
//  AddMealTViewController.swift
//  Cookin
//
//  Created by Yo7ia on 4/12/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import AVFoundation
import UIKit
import FloatRatingView
import Photos
import LKAlertController
import MobileCoreServices
import GMStepper
import DropDown

enum ImageType {
    case ImageOne
    case ImageTwo
    case ImageThree
    case ImageFour
    var Default: ImageType {
        return .ImageOne
    }
}

class AddMealTViewController: BaseTableViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate{
    
    @IBOutlet fileprivate weak var Image1: UIImageView!
    @IBOutlet fileprivate weak var Image2: UIImageView!
    @IBOutlet fileprivate weak var Image3: UIImageView!
    @IBOutlet fileprivate weak var Image4: UIImageView!
    @IBOutlet fileprivate weak var MealName: UITextField!
    @IBOutlet fileprivate weak var MealDescription: UITextView!
    @IBOutlet fileprivate weak var MealCategory: UIButton!
    @IBOutlet fileprivate weak var MealUnitNumber: GMStepper!
    @IBOutlet fileprivate weak var MealPrice: UITextField!
    @IBOutlet fileprivate weak var MealAddBTN: UIButton!
    @IBOutlet fileprivate weak var MealCancelBTN: UIButton!

    var check = true
    var CheckImageType: ImageType = .ImageOne
    var Image1Data:Data?
    var Image2Data:Data?
    var Image3Data:Data?
    var Image4Data:Data?
    var allcategories: [String]! = ["GYM","DIET","SWEETS","SAVORY","TRADITIONAL"]
    let dropDown = DropDown()
    var MealSelectedCategory = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       self.initUI()
        
    }
    
    func initUI()
    {
        self.title = "Add Meal"
        MealAddBTN.Curvyimage()
        MealCancelBTN.Curvyimage()
        MealDescription.addPadding()
        MealDescription.delegate = self
        MealDescription.text = "Add Description"
        
        let singleTap = UITapGestureRecognizer(target: self,action: #selector(EditImageOne))
        singleTap.numberOfTapsRequired = 1
        self.Image1.isUserInteractionEnabled = true
        self.Image1.addGestureRecognizer(singleTap)
        
        
        let singleTapShare = UITapGestureRecognizer(target: self,action: #selector(EditImageTwo))
        singleTapShare.numberOfTapsRequired = 1
        self.Image2.isUserInteractionEnabled = true
        self.Image2.addGestureRecognizer(singleTapShare)
 
        let singleTapShare2 = UITapGestureRecognizer(target: self,action: #selector(EditImageThree))
        singleTapShare2.numberOfTapsRequired = 1
        self.Image3.isUserInteractionEnabled = true
        self.Image3.addGestureRecognizer(singleTapShare2)

        let singleTapShare3 = UITapGestureRecognizer(target: self,action: #selector(EditImageFour))
        singleTapShare3.numberOfTapsRequired = 1
        self.Image4.isUserInteractionEnabled = true
        self.Image4.addGestureRecognizer(singleTapShare3)
        
        dropDown.anchorView = self.MealCategory
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = UIColor.white
        dropDown.dataSource = allcategories
        dropDown.selectionAction = {(index: Int, item: String) in
            self.MealSelectedCategory = item
            self.MealCategory.setTitle(item, for: .normal)
        }

    }
    
    
    
    
    fileprivate func validateAll() -> Bool {
        // Validate email
        if (Image1Data == nil && Image2Data == nil && Image3Data == nil && Image4Data == nil )
        {
            AlertUtility.showErrorAlert("Add At Least One Image For Meal")
            return false

        }
        if (!Validation.isValidUsername(MealName.text!)  ) {
            AlertUtility.showErrorAlert("Enter Meal Name")
            return false
        }
        if (!Validation.isValidUsername(MealDescription.text!)  ) {
            AlertUtility.showErrorAlert("Enter Meal Description")
            return false
        }
        if (!Validation.isValidUsername(MealSelectedCategory)  ) {
            AlertUtility.showErrorAlert("Select Meal Category")
            return false
        }
        if (!Validation.isValidUsername(MealPrice.text!)  ) {
            AlertUtility.showErrorAlert("Enter Meal Price")
            return false
        }
        


        return true
    }
    
    
    
    
    @IBAction func MealCancelBTNClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func CategoryBTNClicked(_ sender: Any) {
        self.dropDown.show()
    }
    @IBAction func MealAddBTNClicked(_ sender: Any) {
        if (!self.validateAll()) {
            return
        }
        UserModel.AddMeal(MealName: MealName.text!, MealDescription: MealDescription.text!, MealPrice: MealPrice.text!, MealUnitNumber: String(Int(MealUnitNumber.value)), MealTypeID: allcategories.index(of: MealSelectedCategory)!+1, ClientId: String(LoggedUserProfile!.id), Weight: 1, isHeat: true, Image1: Image1Data, Image2: Image2Data, Image3: Image3Data, Image4: Image4Data) { (data, error, errCode) in
            if !super.handleNetworkError(data ,msg: error, errorCode: errCode,username: "") {
                self.navigationController!.popViewController(animated: true)
                AlertUtility.showSuccessAlert("Meal Added Successfully")
            }
        }
        
    }
    
    
    func EditImageOne() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageOne
                
                self?.showImagePickerWithSourceType(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageOne
                
                self?.showImagePickerWithSourceType(.camera)
            }
        }
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    func EditImageTwo() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageTwo
                
                self?.showImagePickerWithSourceType(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageTwo
                
                self?.showImagePickerWithSourceType(.camera)
            }
        }
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    func EditImageThree() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageThree
                
                self?.showImagePickerWithSourceType(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageThree
                
                self?.showImagePickerWithSourceType(.camera)
            }
        }
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    func EditImageFour() {
        let sheet = ActionSheet(title: "Select image")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            sheet.addAction("Photo Library", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageFour
                
                self?.showImagePickerWithSourceType(.photoLibrary)
            }
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            sheet.addAction("Camera", style: .default) { [weak self] _ in
                self?.CheckImageType = .ImageFour
                
                self?.showImagePickerWithSourceType(.camera)
            }
        }
        sheet.addAction("Cancel")
        
        sheet.show()
    }
    fileprivate func showImagePickerWithSourceType(_ sourceType: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = sourceType;
        picker.mediaTypes = [kUTTypeImage as String]
        picker.allowsEditing = false
        
        present(picker, animated: true, completion: nil)
    }
    
    func checkCamera(_ sourceType: UIImagePickerControllerSourceType) {
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authStatus {
        case .authorized: showImagePickerWithSourceType(sourceType) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageCameraAccessInitially(sourceType)
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting(sourceType)
        default: alertToEncourageCameraAccessInitially(sourceType)
        }
    }
    func checkLibrary(_ sourceType: UIImagePickerControllerSourceType) {
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
        case .authorized: showImagePickerWithSourceType(sourceType) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageLibraryAccessInitially(sourceType)
        case .notDetermined: alertPromptToAllowLibraryAccessViaSetting(sourceType)
        default: alertToEncourageLibraryAccessInitially(sourceType)
        }
    }
    
    func alertToEncourageLibraryAccessInitially(_ sourceType: UIImagePickerControllerSourceType) {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Library access required for getting photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Library", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowLibraryAccessViaSetting(_ sourceType: UIImagePickerControllerSourceType) {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Library access required for getting photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            PHPhotoLibrary.requestAuthorization({ (status) in
                self.checkLibrary(.photoLibrary)
            })
            
        })
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func alertToEncourageCameraAccessInitially(_ sourceType: UIImagePickerControllerSourceType) {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting(_ sourceType: UIImagePickerControllerSourceType) {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        self.checkCamera(sourceType) } }
            }
            }
        )
        present(alert, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        self.check = false
//        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
        
        let imgg = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        let Data = imgg.jpeg(.low)
        
        
        switch CheckImageType {
        case .ImageOne:
            Image1Data = Data
            self.Image1.image = imgg
            break
        case .ImageTwo:
            Image2Data = Data
            self.Image2.image = imgg
            break
        case .ImageThree:
            Image3Data = Data
            self.Image3.image = imgg
            break
        case .ImageFour:
            Image4Data = Data
            self.Image4.image = imgg
            break
        }
        
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if(self.MealDescription.text == "Add Description") {
            self.MealDescription.text = ""
        }
        MealDescription.becomeFirstResponder() //Optional
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(MealDescription.text == "") {
            self.MealDescription.text = "Add Description"
//            self.MealDescription.textColor = UIColor.lightGray
        }
    }
    
    
}
