//
//  User.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import ObjectMapper
class User : BaseDTO {
    
    
    
    var City = ""
    var username = ""
    var password = ""
    var MobileNumber = ""
    var Email = ""
    var Token = ""
    var BearerToken = ""
    required init?(map: Map) {
        super.init(map: map)
    }
    
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        self.username <- map["username"]
        self.Token <- map["Token"]
        self.MobileNumber <- map["MobileNumber"]
        self.Email <- map["Email"]
        self.City <- map["City"]
        self.BearerToken = "Bearer \(self.Token)"

    }
    
    
}
