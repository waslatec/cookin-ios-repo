//
//  Meal.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

//
//  UserProfile.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import Foundation
import ObjectMapper
class Meal : BaseDTO {
    
    
    
    var MapLat = 0.0
    var MapLng = 0.0
    var Id = ""
    var Images: [String] = [String]()
    var MealTime: Int = 0
    var Name = ""
    var Price: Double = 0.0
    var Rate: Double = 0.0
    var UnitNumber: Int = 0
    var Weight: Double = 0.0
    var MealTypeId: String = ""
    var IsAviliable =  false
    var ClientName = ""
    var imgss = [String]()
    var Categories = [Category]()
    var IsHeat = false
    var ClientID : Int = 0
    var MealDescription = ""
    required init?(map: Map) {
        super.init(map: map)
    }
    
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        self.MapLat <- map["MapLat"]
        self.MapLng <- map["MapLng"]
        self.Id <- map["Id"]
        self.ClientID <- map["ClientID"]
        let keyExists =  map["Images"].isKeyPresent
        
        if keyExists
        {
            self.imgss <- map["Images"]
        }
        else{
            self.imgss <- map["MealImages"]
        }
        self.MealTime <- map["MealTime"]
        self.MealDescription <- map["MealDescription"]
        self.Name <- map["Name"]
        self.Price <- map["Price"]
        self.Rate <- map["Rate"]
        self.UnitNumber <- map["UnitNumber"]
        self.Weight <- map["Weight"]
        self.MealTypeId <- map["MealTypeId"]
        self.IsAviliable <- map["IsAviliable"]
        self.IsHeat <- map["IsHeat"]
        self.Categories <- map["Categories"]
        self.ClientName <- map["ClientName"]
        for i in imgss
        {
            self.Images.append("http://api.coockin.wasltec.com/Uploads/Meals/"+self.Id+"/"+i)
        }

    }
    
    
}
class Category : BaseDTO {
    
    
    
   
    var Id = ""
    var EnglishName = ""
    var ArabicName = ""
    var isDeleted: Bool = false
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        self.Id <- map["Id"]
        self.EnglishName <- map["EnglishName"]
        self.isDeleted <- map["isDeleted"]
        self.ArabicName <- map["ArabicName"]
        
    }
    
    
}
