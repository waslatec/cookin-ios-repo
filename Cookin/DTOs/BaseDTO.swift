//
//  BaseDTO.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseDTO: Mappable {
    var id: Int = 0;
    var type: String = "";
    required init?(map: Map) {
       
    }
    func mapping(map: Map) {
        let keyExists =  map["_id"].isKeyPresent
        
        if keyExists
        {
            self.id <- map["_id"]
        }
        else{
            self.id <- map["id"]
        }
        
        let typeExists = map["type"].isKeyPresent
        
        if typeExists
        {
            self.type <- map["type"]
        }
        else{
            self.type = ""
        }
    }

}
