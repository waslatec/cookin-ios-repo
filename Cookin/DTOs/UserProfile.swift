//
//  UserProfile.swift
//  Cookin
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import Foundation
import ObjectMapper
class UserProfile : BaseDTO {
    
    
    
    var MapLat = 0.0
    var MapLng = 0.0
    var CoverPhotoPath = ""
    var CityId = ""
    var NickName = ""
    var UserName = ""
    var Email = ""
    var BirthDay = ""
    var isCheef = false
    var ProfilePhotoPath = ""
    var Rate = 0
    required init?(map: Map) {
        super.init(map: map)
    }
    
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        self.MapLat <- map["MapLat"]
        self.MapLng <- map["MapLng"]
        self.CoverPhotoPath <- map["CoverPhotoPath"]
        self.Email <- map["Email"]
        self.CityId <- map["CityId"]
        self.ProfilePhotoPath <- map["ProfilePhotoPath"]
        self.NickName <- map["NickName"]
        self.Rate <- map["Rate"]
        self.UserName <- map["UserName"]
        self.isCheef <- map["isCheef"]
        self.BirthDay <- map["BirthDay"]
    }
    
    
}
